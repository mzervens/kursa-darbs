void render()
	{

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);
	//	

		glUseProgram(shaderProgram);


	//	glColorMask(false, true, true, false);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

		// Activates vertex atribute index
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		// defines the format and source location (buffer object) of the vertex data
		//glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		//glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.data.size() * sizeof(float)) / 2));
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	//	glutSolidTorus(40, 200, 20, 30);

		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));

	//	printMatrix(leftFrustrumMtx);
/*
		setPerspectiveMtx(leftFrustrumMtx);
		translate(eyeSeperation / 2, 0.0f, -1815.0f, false);


	//	printMatrix(translationMatrix);

		glColorMask(true, false, false, false);


		glutSetVertexAttribCoord3(0);
		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);
	//	glutSolidCube(400.0f);

		translate(eyeSeperation / 2 + 650, 0.0f, -1655.0f, false);

		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);

		translate(eyeSeperation / 2 - 650, 0.0f, -1955.0f, false);

		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);

		
		glClear(GL_DEPTH_BUFFER_BIT);

		*/
		glDrawElements(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, 36);
		//glDrawElementsBaseVertex(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0, offset);

		/*
		setPerspectiveMtx(rightFrustrumMtx);
		translate(-eyeSeperation / 2, 0.0f, -1815.0f, false);
		glColorMask(false, true, true, false);


		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);


		translate(-eyeSeperation / 2 + 650, 0.0f, -1655.0f, false);

		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);


		translate(-eyeSeperation / 2 - 650, 0.0f, -1955.0f, false);

		glutSolidTorus(40, 200, 20, 30);
		glutWireTorus(40, 200, 20, 30);


		*/

	//	glutSolidCube(400.0f);
		//glDrawElements(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0);
		


		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);


		glUseProgram(0);

		glutSwapBuffers();
		
		
		// If we want to render constantly
		glutPostRedisplay();
	}