#version 330

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;

uniform mat4 perspMtxCamToClip;

uniform mat4 translationMtx;

uniform mat4 cameraMtx;





uniform mat4 rotationXMtx;
uniform mat4 rotationYMtx;
uniform mat4 rotationZMtx;

smooth out vec4 fragColor;

void main()
{
	vec4 translatedPos = translationMtx * position;
	translatedPos = cameraMtx * translatedPos;
	gl_Position = perspMtxCamToClip * translatedPos;
//	fragColor = color;
	fragColor = vec4(0.2, 0.2, 0.6, 0.0);
}
