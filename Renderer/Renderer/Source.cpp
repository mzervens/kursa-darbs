#pragma once
#include <glload/gl_load.hpp>
#include <glload/gl_all.h>



#include <string>
#include <glload/gl_3_3.h>
#include <glload/gl_load.hpp>
#include <GL/freeglut.h>
//#include "Keyboard.cpp"
#include "Window.cpp"
//#include "Renderer.cpp"

#include <iostream>


using namespace std;


void PlaceScen()
{
	// translate to appropriate depth along -Z
	glTranslatef(0.0f, 0.0f, -1800.0f);



	// rotate the scene for viewing
	//	glRotatef(-60.0f, 1.0f, 0.0f, 0.0f);
	//	glRotatef(-45.0f, 0.0f, 0.0f, 1.0f);

	GLfloat * test = new GLfloat[16];

	glGetFloatv(GL_MODELVIEW_MATRIX, test);

	cout << "SCENE" << "\n";

	cout << test[0] << " " << test[4] << " " << test[8] << " " << test[12] << "\n";
	cout << test[1] << " " << test[5] << " " << test[9] << " " << test[13] << "\n";
	cout << test[2] << " " << test[6] << " " << test[10] << " " << test[14] << "\n";
	cout << test[3] << " " << test[7] << " " << test[11] << " " << test[15] << "\n";

	cout << "\n";


	// draw intersecting tori
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 240.0f);
	glRotatef(95.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.2, 0.2, 0.6);
	glutSolidTorus(40, 200, 20, 30);
	glColor3f(0.7f, 0.7f, 0.7f);
	glutWireTorus(40, 200, 20, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(240.0f, 0.0f, 240.0f);
	glColor3f(0.2, 0.2, 0.6);
	glutSolidTorus(40, 200, 20, 30);
//	glutSolidCube(400);
	glutWireDodecahedron();
	glColor3f(0.7f, 0.7f, 0.7f);
	glutWireTorus(40, 200, 20, 30);
	glPopMatrix();
}

class StereoCamera
{
public:
	StereoCamera(
		float Convergence,
		float EyeSeparation,
		float AspectRatio,
		float FOV,
		float NearClippingDistance,
		float FarClippingDistance
		)
	{
		mConvergence = Convergence;
		mEyeSeparation = EyeSeparation;
		mAspectRatio = AspectRatio;
		mFOV = FOV * 3.14159265359f / 180.0f;
		mNearClippingDistance = NearClippingDistance;
		mFarClippingDistance = FarClippingDistance;
	}

	void ApplyLeftFrustum()
	{
		float top, bottom, left, right;

		top = mNearClippingDistance * tan(mFOV / 2);
		bottom = -top;

		float a = mAspectRatio * tan(mFOV / 2) * mConvergence;

		float b = a - mEyeSeparation / 2;
		float c = a + mEyeSeparation / 2;

		left = -b * mNearClippingDistance / mConvergence;
		right = c * mNearClippingDistance / mConvergence;

		// Set the Projection Matrix
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glFrustum(left, right, bottom, top,
			mNearClippingDistance, mFarClippingDistance);

		GLfloat * test = new GLfloat[16];

		glGetFloatv(GL_PROJECTION_MATRIX, test);

		cout << test[0] << " " << test[4] << " " << test[8] << " " << test[12] << "\n";
		cout << test[1] << " " << test[5] << " " << test[9] << " " << test[13] << "\n";
		cout << test[2] << " " << test[6] << " " << test[10] << " " << test[14] << "\n";
		cout << test[3] << " " << test[7] << " " << test[11] << " " << test[15] << "\n";

		cout << "\n";

		// Displace the world to right
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(mEyeSeparation / 2, 0.0f, 0.0f);

		glGetFloatv(GL_MODELVIEW_MATRIX, test);

		cout << test[0] << " " << test[4] << " " << test[8] << " " << test[12] << "\n";
		cout << test[1] << " " << test[5] << " " << test[9] << " " << test[13] << "\n";
		cout << test[2] << " " << test[6] << " " << test[10] << " " << test[14] << "\n";
		cout << test[3] << " " << test[7] << " " << test[11] << " " << test[15] << "\n";

		cout << "\n";
	}

	void ApplyRightFrustum()
	{
		float top, bottom, left, right;

		top = mNearClippingDistance * tan(mFOV / 2);
		bottom = -top;

		float a = mAspectRatio * tan(mFOV / 2) * mConvergence;

		float b = a - mEyeSeparation / 2;
		float c = a + mEyeSeparation / 2;

		left = -c * mNearClippingDistance / mConvergence;
		right = b * mNearClippingDistance / mConvergence;

		// Set the Projection Matrix
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum(left, right, bottom, top,
			mNearClippingDistance, mFarClippingDistance);


		// Displace the world to left
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-mEyeSeparation / 2, 0.0f, 0.0f);
	}

private:
	float mConvergence;
	float mEyeSeparation;
	float mAspectRatio;
	float mFOV;
	float mNearClippingDistance;
	float mFarClippingDistance;

};


// main rendering function
void DrawScene(GLvoid)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set up the stereo camera system
	StereoCamera cam(
		2000.0f,     // Convergence 
		65.0f,       // Eye Separation
		1.77777f,     // Aspect Ratio
		45.0f,       // FOV along Y in degrees
		10.0f,       // Near Clipping Distance
		20000.0f);   // Far Clipping Distance

	cam.ApplyLeftFrustum();
	glColorMask(true, false, false, false);

	PlaceScen();

	glClear(GL_DEPTH_BUFFER_BIT);

	cam.ApplyRightFrustum();
	glColorMask(false, true, true, false);

	PlaceScen();

	glColorMask(true, true, true, true);

	glutSwapBuffers();
	//	glutPostRedisplay();
}


int main(int argc, char * argv[])
{
	Window w(1920, 1080, GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH | GLUT_STENCIL, argc, argv, "Hello World!");


	/*	Renderer r(3, 3, true, indexedDta);

	Keyboard k(r.getReference());

	w.bindRenderer(r.getReference());

	r.startLoop();

	*/


	glutInitContextVersion(2, 1);
	glload::LoadFunctions();

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f);



	glutDisplayFunc(DrawScene);

	glutMainLoop();
}