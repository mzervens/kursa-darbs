#pragma once

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>

using namespace std;

class Light
{
public:
	GLuint lightPosUnif;
	GLuint lightColUnif;

	glm::vec3 position;
	glm::vec3 color;


	Light(glm::vec3 pos = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 col = glm::vec3(1.0f, 1.0f, 1.0f))
	{
		position = pos;
		color = col;

	}




};