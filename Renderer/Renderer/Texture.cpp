#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <glimg\glimg.h>
#include <GL/freeglut.h>

using namespace std;

class Texture
{
	

public:

	GLuint textureHandle;
	int width;
	int height;

	Texture()
	{
		textureHandle = -1;

	}


	/**
	 *	Creates empty texture with provided dimensions
	 */
	Texture(int width, int height, GLenum filter = NULL)
	{
		this->width = width;
		this->height = height;

		glGenTextures(1, &textureHandle);
		glBindTexture(GL_TEXTURE_2D, textureHandle);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		if (filter == NULL)
		{
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else
		{
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
		}


		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);


		glBindTexture(GL_TEXTURE_2D, 0);
	}


	Texture(string &fileName)
	{
		textureHandle = 0;

		try
		{
			auto_ptr<glimg::ImageSet> pImgSet(glimg::loaders::stb::LoadFromFile(fileName.c_str()));
			glimg::SingleImage image = pImgSet->GetImage(0, 0, 0);
			glimg::Dimensions dims = image.GetDimensions();
		//	textureHandle = glimg::CreateTexture(pImgSet.get(), 0);

			glGenTextures(1, &textureHandle);
			glBindTexture(GL_TEXTURE_2D, textureHandle);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dims.width, dims.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.GetImageData());


			glBindTexture(GL_TEXTURE_2D, 0);


			this->width = dims.width;
			this->height = dims.height;
		}
		catch (glimg::loaders::stb::StbLoaderException &e)
		{
			//Image file loading failed.
			cout << "Image file " << fileName << " loading failure in class Texture \n";
			cout << e.what() << "\n";
			return;
			
		}
		
	/*	catch (glimg::TextureGenerationException &e)
		{
			//Texture creation failed.
			cout << "Texture creation failed with image file " << fileName << " in class Texture \n";
			cout << e.what() << "\n";
			return;
		}
		*/

	}


	Texture loadRelative(string path)
	{
		string strFilename = __FILE__;
		string::size_type pos = strFilename.find_last_of("\\/");
		strFilename = strFilename.substr(0, pos);

//		strFilename = "";

		strFilename += path;

		return Texture(strFilename);
	}

	void bind()
	{
		if (textureHandle == -1)
		{
			cout << "Texture not initialized" << endl;
			return;
		}

		glBindTexture(GL_TEXTURE_2D, textureHandle);

	}

	void unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}


};