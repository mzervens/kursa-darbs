#pragma once
#include <vector>
#include <string>
#include <glload/gl_3_3.h>
#include <glload/gl_load.hpp>
#include <GL/freeglut.h>
#include "Renderer.cpp"
#include  <iostream>

using namespace std;

class Mouse;

static Mouse * mouseInstance;

class Mouse
{
	Renderer * rendererInst;

	float mouseX;
	float mouseY;

	float sensitivity;

public:

	Mouse(Renderer * instance)
	{
		mouseInstance = this;
		rendererInst = instance;

		mouseX = mouseY = 0.0f;

		sensitivity = 0.05;

		glutSetCursor(GLUT_CURSOR_NONE);

		glutPassiveMotionFunc(mouseCallback);

	}

	static void mouseCallback(int x, int y)
	{
		mouseInstance->mouseMovement(x, y);
	}

	void mouseMovement(int x, int y)
	{
		if (x == mouseX && y == mouseY)
		{
			return;
		}

		float yaw = x - mouseX;
		float pitch = mouseY - y;

//		mouseX = x;
//		mouseY = y;


		GLint centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
		GLint centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

		mouseX = centerX;
		mouseY = centerY;

		glutWarpPointer(mouseX, mouseY);


		yaw *= sensitivity;
		pitch *= sensitivity;


		// So we cant flip the world upside down
		if (pitch > 90.0f)
		{
			pitch = 90.0f;
		}
		if (pitch < -90.0f)
		{
			pitch = -90.0f;
		}

		rendererInst->orientateCamera(yaw, pitch);

	}


};