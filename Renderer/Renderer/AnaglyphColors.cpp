#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace std;

/**
 *	Anaglyph color calculation matrices, information taken from this webpage 
 *  http://www.3dtv.at/knowhow/anaglyphcomparison_en.aspx
 */
class AnaglyphColors
{
public:
	glm::mat3 trueAnaglyphLeft;
	glm::mat3 trueAnaglyphRight;

	glm::mat3 greyAnaglyphLeft;
	glm::mat3 greyAnaglyphRight;

	glm::mat3 colorAnaglyphLeft;
	glm::mat3 colorAnaglyphRight;

	glm::mat3 halfColorAnaglyphLeft;
	glm::mat3 halfColorAnaglyphRight;

	glm::mat3 optimizedAnaglyphLeft;
	glm::mat3 optimizedAnaglyphRight;

	int currActColorType;


	AnaglyphColors()
	{
		initTrueAnaglyphs();
		initGreyAnaglyphs();
		initColorAnaglyphs();
		initHalfColorAnaglyphs();
		initOptimizedAnaglyphs();

		currActColorType = 0;
	}

	void setActiveTrueAnaglyph()
	{
		currActColorType = 0;
	}

	void setActiveGreyAnaglyph()
	{
		currActColorType = 1;
	}

	void setActiveColorAnaglyph()
	{
		currActColorType = 2;
	}

	void setActiveHalfColorAnaglyph()
	{
		currActColorType = 3;
	}

	void setActiveOptimizedAnaglyph()
	{
		currActColorType = 4;
	}

	void setActiveGIMPColorScheme()
	{
		currActColorType = 5;
	}

private:

	void initTrueAnaglyphs()
	{
		trueAnaglyphLeft[0].x = 0.299f;
		trueAnaglyphLeft[0].y = 0.0f;
		trueAnaglyphLeft[0].z = 0.0f;

		trueAnaglyphLeft[1].x = 0.587f;
		trueAnaglyphLeft[1].y = 0.0f;
		trueAnaglyphLeft[1].z = 0.0f;

		trueAnaglyphLeft[2].x = 0.114f;
		trueAnaglyphLeft[2].y = 0.0f;
		trueAnaglyphLeft[2].z = 0.0f;


		trueAnaglyphRight[0].x = 0.0f;
		trueAnaglyphRight[0].y = 0.0f;
		trueAnaglyphRight[0].z = 0.299f;

		trueAnaglyphRight[1].x = 0.0f;
		trueAnaglyphRight[1].y = 0.0f;
		trueAnaglyphRight[1].z = 0.587f;

		trueAnaglyphRight[2].x = 0.0f;
		trueAnaglyphRight[2].y = 0.0f;
		trueAnaglyphRight[2].z = 0.114f;

		return;
	}

	void initGreyAnaglyphs()
	{
		greyAnaglyphLeft[0].x = 0.299f;
		greyAnaglyphLeft[0].y = 0.0f;
		greyAnaglyphLeft[0].z = 0.0f;

		greyAnaglyphLeft[1].x = 0.587f;
		greyAnaglyphLeft[1].y = 0.0f;
		greyAnaglyphLeft[1].z = 0.0f;

		greyAnaglyphLeft[2].x = 0.114f;
		greyAnaglyphLeft[2].y = 0.0f;
		greyAnaglyphLeft[2].z = 0.0f;


		greyAnaglyphRight[0].x = 0.0f;
		greyAnaglyphRight[0].y = 0.299f;
		greyAnaglyphRight[0].z = 0.299f;

		greyAnaglyphRight[1].x = 0.0f;
		greyAnaglyphRight[1].y = 0.587f;
		greyAnaglyphRight[1].z = 0.587f;

		greyAnaglyphRight[2].x = 0.0f;
		greyAnaglyphRight[2].y = 0.114f;
		greyAnaglyphRight[2].z = 0.114f;

		return;
	}

	void initColorAnaglyphs()
	{
		colorAnaglyphLeft[0].x = 1.0f;
		colorAnaglyphLeft[0].y = 0.0f;
		colorAnaglyphLeft[0].z = 0.0f;

		colorAnaglyphLeft[1].x = 0.0f;
		colorAnaglyphLeft[1].y = 0.0f;
		colorAnaglyphLeft[1].z = 0.0f;

		colorAnaglyphLeft[2].x = 0.0f;
		colorAnaglyphLeft[2].y = 0.0f;
		colorAnaglyphLeft[2].z = 0.0f;


		colorAnaglyphRight[0].x = 0.0f;
		colorAnaglyphRight[0].y = 0.0f;
		colorAnaglyphRight[0].z = 0.0f;

		colorAnaglyphRight[1].x = 0.0f;
		colorAnaglyphRight[1].y = 1.0f;
		colorAnaglyphRight[1].z = 0.0f;

		colorAnaglyphRight[2].x = 0.0f;
		colorAnaglyphRight[2].y = 0.0f;
		colorAnaglyphRight[2].z = 1.0f;

		return;
	}

	void initHalfColorAnaglyphs()
	{
		halfColorAnaglyphLeft[0].x = 0.299f;
		halfColorAnaglyphLeft[0].y = 0.0f;
		halfColorAnaglyphLeft[0].z = 0.0f;

		halfColorAnaglyphLeft[1].x = 0.587f;
		halfColorAnaglyphLeft[1].y = 0.0f;
		halfColorAnaglyphLeft[1].z = 0.0f;

		halfColorAnaglyphLeft[2].x = 0.114f;
		halfColorAnaglyphLeft[2].y = 0.0f;
		halfColorAnaglyphLeft[2].z = 0.0f;


		halfColorAnaglyphRight[0].x = 0.0f;
		halfColorAnaglyphRight[0].y = 0.0f;
		halfColorAnaglyphRight[0].z = 0.0f;

		halfColorAnaglyphRight[1].x = 0.0f;
		halfColorAnaglyphRight[1].y = 1.0f;
		halfColorAnaglyphRight[1].z = 0.0f;

		halfColorAnaglyphRight[2].x = 0.0f;
		halfColorAnaglyphRight[2].y = 0.0f;
		halfColorAnaglyphRight[2].z = 1.0f;

		return;
	}

	void initOptimizedAnaglyphs()
	{
		optimizedAnaglyphLeft[0].x = 0.0f;
		optimizedAnaglyphLeft[0].y = 0.0f;
		optimizedAnaglyphLeft[0].z = 0.0f;

		optimizedAnaglyphLeft[1].x = 0.7f;
		optimizedAnaglyphLeft[1].y = 0.0f;
		optimizedAnaglyphLeft[1].z = 0.0f;

		optimizedAnaglyphLeft[2].x = 0.3f;
		optimizedAnaglyphLeft[2].y = 0.0f;
		optimizedAnaglyphLeft[2].z = 0.0f;


		optimizedAnaglyphRight[0].x = 0.0f;
		optimizedAnaglyphRight[0].y = 0.0f;
		optimizedAnaglyphRight[0].z = 0.0f;

		optimizedAnaglyphRight[1].x = 0.0f;
		optimizedAnaglyphRight[1].y = 1.0f;
		optimizedAnaglyphRight[1].z = 0.0f;

		optimizedAnaglyphRight[2].x = 0.0f;
		optimizedAnaglyphRight[2].y = 0.0f;
		optimizedAnaglyphRight[2].z = 1.0f;

		return;
	}

};