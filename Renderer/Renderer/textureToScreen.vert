#version 330

layout (location = 0) in vec4 position;
layout (location = 2) in vec2 texCoords;

out vec2 colorCoord;

void main()
{
	colorCoord = texCoords;
	gl_Position = position;
}
