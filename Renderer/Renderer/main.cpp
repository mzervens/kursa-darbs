#pragma once
#include <string>

#include "Keyboard.cpp"
#include "Mouse.cpp"
#include "Window.cpp"
#include "Renderer.cpp"
#include "Quaternion.cpp"
#include "ObjParser.cpp"

#include <iostream>


using namespace std;

glm::vec3 getNormal(glm::vec3 triangle[])
{
	Quaternion q;

	glm::vec3 u = triangle[0] - triangle[1];
	glm::vec3 v = triangle[2] - triangle[1];


	glm::vec3 result = q.crossProduct(u, v);

	return result;
}


int main(int argc, char * argv[])
{

	Window w(GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH | GLUT_STENCIL | GLUT_STEREO | GLUT_ACCUM, argc, argv, "Anaglyphs");

	RenderObject o;
	RenderObject o2;
	RenderObject treeBark;
	RenderObject treeLeafs;
	RenderObject torus;
	RenderObject knotTorus;

	ObjParser parser = ObjParser();
	parser.loadObj("\\meshes\\suzane.obj", &o);
	parser.loadObj("\\meshes\\fence.obj", &o2);
//	parser.loadObj("\\meshes\\treeBark.obj", &treeBark);
//	parser.loadObj("\\meshes\\treeLeaves.obj", &treeLeafs);
	parser.loadObj("\\meshes\\torus.obj", &torus);
	parser.loadObj("\\meshes\\knotTorus.obj", &knotTorus);

	cout << "house mesh dta " << o.dataOffset << " " << o.normalsOffset << " " << o.textureCoordOffset << endl;


	RenderObject indexedDta;

	indexedDta.dataOffset = 56;// 112;
	indexedDta.normalsOffset = 56 + 16 + (6 * 6 * 4);

	indexedDta.addVertexData(vector<float>{

		   // Texture coords
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,


			// Plane
			0.5f, -1.0f, -0.5f, 1.0f,
			0.5f, -1.0f, 0.5f, 1.0f,
			-0.5f, -1.0f, 0.5f, 1.0f,
			-0.5f, -1.0f, -0.5f, 1.0f,
		/*	0.5f * 10000, -500.3f, -0.5f * 10000, 1.0f,
			0.5f * 10000, -500.3f, 0.5f * 10000, 1.0f,
			-0.5f * 10000, -500.3f, 0.5f * 10000, 1.0f,
			-0.5f * 10000, -500.3f, -0.5f * 10000, 1.0f,
			*/

			// Unit cube
			-1.0f, -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 1.0f,

			-1.0f, 1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,

			-1.0f, -1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f,


			-1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f,

			-1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,

			1.0f, -1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f,

			// Normals
			// For plane
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,


			// For cube
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,


			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			// front
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			// back
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,

			// left
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,

			// right
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f


			

	});

	indexedDta.addPositionData(vector<unsigned int>{

		// Plane
		24 - 10, 25 - 10, 26 - 10,
		24 - 10, 26 - 10, 25 - 10,
		26 - 10, 27 - 10, 24 - 10,
		26 - 10, 24 - 10, 27 - 10,


		0 + 8, 1 + 8, 2 + 8,
		0 + 8, 2 + 8, 1 + 8,
		2 + 8, 3 + 8, 0 + 8,
		2 + 8, 0 + 8, 3 + 8,

			// Top
			4 + 8, 6 + 8, 7 + 8,
			4 + 8, 5 + 8, 6 + 8,

		// Bottom
		0 + 8, 3 + 8, 2 + 8,
		2 + 8, 1 + 8, 0 + 8,

		// Front
		//1, 5, 4,
		//4, 0, 1,
		8 + 8, 9 + 8, 10 + 8,
		10 + 8, 11 + 8, 8 + 8,

		// Back
		12 + 8, 13 + 8, 14 + 8,
		14 + 8, 15 + 8, 12 + 8,

		// Left
		16 + 8, 17 + 8, 18 + 8,
		18 + 8, 19 + 8, 16 + 8,

		//right
		20 + 8, 21 + 8, 22 + 8,
		22 + 8, 23 + 8, 20 + 8,
		


	});


	RenderObject meshes[7];
	meshes[0] = indexedDta;
	meshes[1] = o;
	meshes[2] = o2;
	meshes[3] = treeBark;
	meshes[4] = treeLeafs;
	meshes[5] = torus;
	meshes[6] = knotTorus;
	
	Renderer r(3, 3, true, meshes);
	//r.house = &o;

	Keyboard k(r.getReference());

	Mouse m(r.getReference());

	w.bindRenderer(r.getReference());

	r.startLoop();


}