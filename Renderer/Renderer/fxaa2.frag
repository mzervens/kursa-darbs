#version 330

in vec2 colorCoord;
out vec4 outputColor;

uniform sampler2D colorTexture;
uniform vec2 textureSizeInv;


void main()
{
	float FXAAMaxSpan = 8.0;
	float FXAAReduceMin = 1.0/128.0;
	float FXAAReduceMul = 1.0/8.0;

	// Gets the lumanacity of the color by doing dot product with it
	vec3 luma = vec3(0.299, 0.587, 0.114);

	// Since image space top left corner is 0,0 then to go left by a pixel we have to substract one pixel unit from the x component
	// To go up by one pixel we have to substract one pixel unit from the y component
	vec2 pixelUnit = vec2(1.0, 1.0) * textureSizeInv;
	vec2 pixUnitTopRight = vec2(pixelUnit.x, -pixelUnit.y);
	vec2 pixUnitBotLeft = vec2(-pixelUnit.x, pixelUnit.y);
	
	
	float lumCurrPix = dot(luma, texture(colorTexture, colorCoord.xy).xyz);


	float lumTopLeftPix = dot(luma, texture(colorTexture, colorCoord.xy - pixelUnit).xyz);
	float lumTopRightPix = dot(luma, texture(colorTexture, colorCoord.xy + pixUnitTopRight).xyz);
	float lumBotLeftPix = dot(luma, texture(colorTexture, colorCoord.xy + pixUnitBotLeft).xyz);
	float lumBotRightPix = dot(luma, texture(colorTexture, colorCoord.xy + pixelUnit).xyz);
	
	
	float lumMin = min(lumCurrPix, min(min(lumTopLeftPix, lumTopRightPix), min(lumBotLeftPix, lumBotRightPix)));
	float lumMax = max(lumCurrPix, max(max(lumTopLeftPix, lumTopRightPix), max(lumBotLeftPix, lumBotRightPix)));


	vec2 blurDir;
	// Inverted because of the texture coord top left corner
	blurDir.x = -(lumTopLeftPix + lumTopRightPix) - (lumBotLeftPix + lumBotRightPix);
	blurDir.y = (lumTopLeftPix + lumBotLeftPix) - (lumTopRightPix + lumBotRightPix);
	// This gets the direction where to blur but we cant really tell how far do we blur exactly


	float minAbsDirComp = min(abs(blurDir.x), abs(blurDir.y));
	float dirReduce = max((lumTopLeftPix + lumTopRightPix + lumBotLeftPix + lumBotRightPix) * (FXAAReduceMul * 0.25), FXAAReduceMin);


	float blurDirAdjstmnt = 1.0/(minAbsDirComp + dirReduce);

	blurDir = blurDir * blurDirAdjstmnt;

	blurDir = clamp(blurDir, vec2(-FXAAMaxSpan, -FXAAMaxSpan), vec2(FXAAMaxSpan, FXAAMaxSpan)) * textureSizeInv;

	
	vec3 result1 = (1.0/2.0) * (texture(colorTexture, colorCoord.xy + (blurDir * vec2(1.0/3.0 - 0.5))).xyz +
								texture(colorTexture, colorCoord.xy + (blurDir * vec2(2.0/3.0 - 0.5))).xyz);

	vec3 result2 = result1 * (1.0/2.0) + (1.0/4.0) * (texture(colorTexture, colorCoord.xy + (blurDir * vec2(-0.5))).xyz +
													  texture(colorTexture, colorCoord.xy + (blurDir * vec2(0.5))).xyz);

	float lumRes2 = dot(luma, result2);


	if(lumRes2 < lumMin || lumRes2 > lumMax)
	{
		outputColor = vec4(result1, 1.0);
	}
	else
	{
		outputColor =  vec4(result2, 1.0);
	}

}