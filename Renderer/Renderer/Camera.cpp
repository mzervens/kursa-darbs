#pragma once

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Quaternion.cpp"

using namespace std;

enum rotationType
{
	ROTATION_X = 0,
	ROTATION_Y = 1,
	ROTATION_Z = 2,

};

class Camera
{
	glm::vec3 upAxis;
	glm::vec3 camPos;
	Quaternion camDirRot;
	glm::vec3 camDir;
	float scale;
	Quaternion rotation;



   /**
	*  X1  Y1  Z1
	*  lx  ly  lz
	*  rx  ry  rz
	*/
	glm::vec3 crossProduct(const glm::vec3 l, const glm::vec3 r) const
	{
		glm::vec3 result;
		result.x = l.y * r.z - l.z * r.y;
		result.y = -(l.x * r.z - l.z * r.x);
		result.z = l.x * r.y - l.y * r.x;

		return result;
	}

public:
	Camera()
	{
		camPos = glm::vec3(0.0f, 0.0f, 0.0f);
		camDir = glm::vec3(0.0f, 0.0f, -1.0f);
		camDirRot = Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
		upAxis = glm::vec3(0.0f, 1.0f, 0.0f);

		//scale = 0.11f;
		//scale = 7.7f;
		scale = 55.0f;
	}

	glm::mat4 getViewMtx()
	{
		glm::mat4 viewMtx;
		glm::mat4 transl = glm::mat4();
		
		viewMtx = camDirRot.toMtx();

		// Translation position
		transl[3].x = camPos.x;
		transl[3].y = camPos.y;
		transl[3].z = camPos.z;

		viewMtx = viewMtx * transl;

		/*// Translation position
		viewMtx[3].x = camPos.x;
		viewMtx[3].y = camPos.y;
		viewMtx[3].z = camPos.z;
		*/

		return viewMtx;
	}

	void moveToPosition(glm::vec3 pos)
	{
		camPos = pos;
	}

	void setRotation(Quaternion q)
	{
		camDirRot = q;
	}

	// We are moving along the camera direction vector
	void moveForward()
	{
		camPos -= camDir * scale;
	}

	void moveBackward()
	{
		camPos += camDir * scale;
	//	cout << "camera position " << camPos.x << " " << camPos.y << " " << camPos.z << endl;
	}

	void moveLeft()
	{
		camPos -= crossProduct(camDir, upAxis) * scale;
	}
	void moveRight()
	{
		camPos += crossProduct(camDir, upAxis) * scale;
	}


	void yaw(float angle)
	{
		Quaternion rotY(glm::vec3(0.0f, 1.0f, 0.0f), angle);
		camDirRot = rotY * camDirRot;

	}

	void pitch(float angle)
	{
		Quaternion rotX(glm::vec3(1.0f, 0.0f, 0.0f), angle);
		camDirRot = camDirRot * rotX;
	//	cout << "quaternion " << endl;
	//	camDirRot.print();
	}


	void orientate(float yaw, float pitch)
	{
		this->yaw(yaw);

		this->pitch(pitch);

	}


	glm::mat4 getRotationMtx(float angle, rotationType axis)
	{
		glm::vec3 vector;

		if (axis == rotationType::ROTATION_X)
		{
			vector = glm::vec3(1.0f, 0.0f, 0.0f);
		}
		else if (axis == rotationType::ROTATION_Y)
		{
			vector = glm::vec3(0.0f, 1.0f, 0.0f);
		}
		else
		{
			vector = glm::vec3(0.0f, 0.0f, 1.0f);
		}

		rotation = Quaternion(vector, angle);

		return rotation.toMtx();
	}


};