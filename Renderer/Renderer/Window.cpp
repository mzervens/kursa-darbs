#pragma once
#include "Renderer.cpp"
#include <string>
#include <glload/gl_3_3.h>
#include <glload/gl_load.hpp>
#include <GL/freeglut.h>
#include <iostream>



using namespace std;

class Window;

static Window * windowInstance;

class Window
{
private:
	int windowHandle;
	bool fullScreen;
	int width;
	int height;
	

public:
	Renderer * rendererHandle;

	Window(unsigned int displayMode, int argc, char * argv[], string name, bool defaultSize = false)
	{
		int defWidth = 1920;
		int defHeight = 1080;
		windowInstance = this;
		// Initializes GLUT (OpenGL Utility Toolkit)
		glutInit(&argc, argv);
		int width;
		int height;

		if (defaultSize == true)
		{
			width = defWidth;
			height = defHeight;
		}
		else
		{
			width = glutGet(GLUT_SCREEN_WIDTH);
			height = glutGet(GLUT_SCREEN_HEIGHT);
		}
		fullScreen = false;

		this->width = width;
		this->height = height;

		glutInitDisplayMode(displayMode);
		glutInitWindowSize(width, height);
		glutInitWindowPosition(0, 0);

		windowHandle = glutCreateWindow(name.c_str());

		glutReshapeFunc(Window::reshapeCallback);

	}
	~Window()
	{
		glutDestroyWindow(windowHandle);
	}

	void bindRenderer(Renderer * r)
	{
		rendererHandle = r;
	}

	void static reshapeCallback(int width, int height)
	{
		windowInstance->renderingViewportResh(width, height);
	}


	/**
	 * Changes the OpenGL rendering viewport size
	 */
	void renderingViewportResh(int width, int height)
	{
		cout << "width " << width << " height " << height << "\n";
		glViewport(0, 0, (GLsizei) width, (GLsizei) height);
		float aspect = (float) width / height;
	//	rendererHandle->setAspectRatio((float) width / height);
		rendererHandle->changeAnaglyphFrustrums(aspect);
		this->width = width;
		this->height = height;
	}

	void toggleFullScreen()
	{

		if (fullScreen)
		{
			glutReshapeWindow(width, height);
			glutPositionWindow(0, 0);
		}
		else
		{
			glutFullScreen();
		}


		fullScreen = !fullScreen;


	}





};