#pragma once

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

class Quaternion
{
	glm::vec4 q;
	float magnitude;

	void calcMagnitude()
	{
		// sqrt (x^2 + y^2 + z^2 + w^2)
		float squaredSum = (q.x * q.x) + (q.y * q.y) + (q.z * q.z) + (q.w * q.w);

		if (squaredSum == 1.0f)
		{
			magnitude = squaredSum;
			return;
		}

		magnitude = sqrt(squaredSum);
	}

	float degToRad(float deg)
	{
		float degToRad = 3.14159f / 180.0f;
		return deg * degToRad;
	}


public:
	void normalize()
	{
		calcMagnitude();

		if (magnitude == 1.0f)
		{
			return;
		}

		q.x /= magnitude;
		q.y /= magnitude;
		q.z /= magnitude;
		q.w /= magnitude;
	}

	/**
	 *	X1	Y1	Z1
	 *  lx  ly  lz
	 *  rx  ry	rz
	 */
	glm::vec3 crossProduct(const glm::vec3 l, const glm::vec3 r) const
	{
		glm::vec3 result;
		result.x = l.y * r.z - l.z * r.y;
		result.y = -(l.x * r.z - l.z * r.x);
		result.z = l.x * r.y - l.y * r.x;

		return result;
	}



	Quaternion()
	{
		q.x = q.y = q.z = 0.0f;
		q.w = 1.0f;
	
		magnitude = 1.0f;
	}

	Quaternion(float x, float y, float z, float w)
	{
		q.x = x;
		q.y = y;
		q.z = z;
		q.w = w;

		calcMagnitude();
	}

	Quaternion(glm::vec3 v)
	{
		q.x = v.x;
		q.y = v.y;
		q.z = v.z;
		q.w = 0.0f;

		calcMagnitude();
	}

	Quaternion(const glm::vec3 &v, float angle)
	{
		float angleRad = degToRad(angle / 2);
		Quaternion r(v);
		r.normalize();

		float sinAngle = sin(angleRad);

		q.x = r.q.x * sinAngle;
		q.y = r.q.y * sinAngle;
		q.z = r.q.z * sinAngle;
		q.w = cos(angleRad);
		
		normalize();
	}

	void copy(const Quaternion * q)
	{
		this->q = q->q;
		this->magnitude = q->magnitude;
	}

	void conjugate()
	{
		q.x = -q.x;
		q.y = -q.y;
		q.z = -q.z;
	}

	Quaternion getConjugate() const
	{
		Quaternion c;
		c.copy(this);

		c.conjugate();

		return c;
	}

	/**
	 *  Quaternion multiplication like with matrices is not commutative!
	 *	Quaternion multiplication is defined like so:
	 *
	 *  lQ = lQ.w + vec3(lQ.x, lQ.y, lQ.z)		rQ = rQ.w + vec3(rQ.x, rQ.y, rQ.z)
	 *
	 *	resultQ = w + v
	 *
	 *	w = lQ.w * rQ.w - (lQ.x * rQ.x + lQ.y * rQ.y + lQ.z * rQ.z)
	 *
	 *	v = lQ.w * vec3(rQ.x, rQ.y, rQ.z) + rQ.w * vec3(lQ.x, lQ.y, lQ.z) + vec3(lQ.x, lQ.y, lQ.z) X vec3(rQ.x, rQ.y, rQ.z)
	 */
	Quaternion operator* (const Quaternion &rightQ) const
	{
		float w = q.w * rightQ.q.w - (q.x * rightQ.q.x + q.y * rightQ.q.y + q.z * rightQ.q.z);
		
		glm::vec3 v = glm::vec3(rightQ.q.x * q.w, rightQ.q.y * q.w, rightQ.q.z * q.w);
		v += glm::vec3(q.x * rightQ.q.w, q.y * rightQ.q.w, q.z * rightQ.q.w);
		v += crossProduct(glm::vec3(q.x, q.y, q.z), glm::vec3(rightQ.q.x, rightQ.q.y, rightQ.q.z));

		/*float x = q.w * rightQ.q.x + q.x * rightQ.q.w + q.y * rightQ.q.z - q.z * rightQ.q.y;
		float y = q.w * rightQ.q.y + q.y * rightQ.q.w + q.z * rightQ.q.x - q.x * rightQ.q.z;
		float z = q.w * rightQ.q.z + q.z * rightQ.q.w + q.x * rightQ.q.y - q.y * rightQ.q.x;
		float ww = q.w * rightQ.q.w - q.x * rightQ.q.x - q.y * rightQ.q.y - q.z * rightQ.q.z;

		float xx = q.w*rightQ.q.x + q.x*rightQ.q.w + q.y*rightQ.q.z - q.z*rightQ.q.y;
		float yy = q.w*rightQ.q.y - q.x*rightQ.q.z + q.y*rightQ.q.w + q.z*rightQ.q.x;
		float zz = q.w*rightQ.q.z + q.x*rightQ.q.y - q.y*rightQ.q.x + q.z*rightQ.q.w;
		float www = q.w*rightQ.q.w - q.x*rightQ.q.x - q.y*rightQ.q.y - q.z*rightQ.q.z;


		cout << "DBG" << "\n";
		cout << x << " " << y << " " << z << " " << ww << "\n";

		cout << "DBG2" << "\n";
		cout << xx << " " << yy << " " << zz << " " << www << "\n";
		*/


		return Quaternion(v.x, v.y, v.z, w);
	}

	/**
	 * This operation applies quarternion rotation to the given vector
	 *
	 * The rotation operation is defined like so:
	 *
	 * rotV = Quarternion * Quarternion(v.x, v.y, v.z, 0) * conjugate(Quarternion)
	 */
	glm::vec3 operator* (const glm::vec3 &v) const
	{
		glm::vec3 result;
		Quaternion vQ(v);
		vQ.normalize();

		Quaternion rQ = vQ * getConjugate();
		rQ = *this * rQ;

		result.x = rQ.q.x;
		result.y = rQ.q.y;
		result.z = rQ.q.z;

		return result;

	}

	glm::vec3 getVec3()
	{
		return glm::vec3(q.x, q.y, q.z);
	}

	/**
	 * If the Quaternion is normalized then it can be converted to matrix form like so:
	 * 
	 * 1-2y^2-2z^2   2xy-2wz   2xz+2wy   0
	 * 2xy+2wz   1-2x^2-2z^2   2yz+2wx   0
	 * 2xz-2wy   2yz-2wx   1-2x^2-2y^2   0
	 * 0             0         0       1
	 */
	glm::mat4 toMtx()
	{
		glm::mat4 mtx;

		normalize();

		mtx[0].x = 1 - 2 * (q.y * q.y) - 2 * (q.z * q.z);
		mtx[1].y = 1 - 2 * (q.x * q.x) - 2 * (q.z * q.z);
		mtx[2].z = 1 - 2 * (q.x * q.x) - 2 * (q.y * q.y);

		float xy2, xz2, wz2, wy2, yz2, wx2;

		xy2 = 2 * q.x * q.y;
		xz2 = 2 * q.x * q.z;
		wz2 = 2 * q.w * q.z;
		wy2 = 2 * q.w * q.y;
		yz2 = 2 * q.y * q.z;
		wx2 = 2 * q.w * q.x;


		mtx[0].y = xy2 + wz2;
		mtx[0].z = xz2 - wy2;

		mtx[1].x = xy2 - wz2;
		mtx[1].z = yz2 - wx2;

		mtx[2].x = xz2 + wy2;
		mtx[2].y = yz2 + wx2;

		return mtx;
	}


	void print()
	{
		cout << q.x << " " << q.y << " " << q.z << " " << q.w << "\n";
	}
};