#version 330

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 normal;

uniform mat4 perspMtxCamToClip;

uniform mat4 translationMtx;

uniform mat4 cameraMtx;



uniform int perspMtxType;

uniform int drawTex;
out vec2 colorCoord;
out flat int drawTexFrag;

uniform mat4 rotationXMtx;
uniform mat4 rotationYMtx;
uniform mat4 rotationZMtx;



uniform int activeAnaglyphColor;

uniform mat3 trueAnaglyphLftClrMtx;
uniform mat3 trueAnaglyphRgtClrMtx;
		
		
uniform mat3 greyAnaglyphLftClrMtx;
uniform mat3 greyAnaglyphRgtClrMtx;


uniform mat3 colorAnaglyphLftClrMtx;
uniform mat3 colorAnaglyphRgtClrMtx;


uniform mat3 halfColorAnaglyphLftClrMtx;
uniform mat3 halfColorAnaglyphRgtClrMtx;


uniform mat3 optimizedAnaglyphLftClrMtx;
uniform mat3 optimizedAnaglyphRgtClrMtx;

out mat3 textureColorMtx;
out flat int anaglyphUsed;

out mat3 activeClrLeft;
out mat3 activeClrRight;

smooth out vec4 fragColor;


uniform int combineRender;
out flat int combineRenderFr;


// Lighting
out vec3 unmodVerPos;
out vec3 fragNormal;

out flat int usingGIMPScheme;

void main()
{
	mat3 activeAnaglyphClrL;
	mat3 activeAnaglyphClrR;

	vec3 modifiedAnaglyphClr;

	if(activeAnaglyphColor == 0)
	{
		activeAnaglyphClrL = trueAnaglyphLftClrMtx;
		activeAnaglyphClrR = trueAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 1)
	{
		activeAnaglyphClrL = greyAnaglyphLftClrMtx;
		activeAnaglyphClrR = greyAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 2)
	{
		activeAnaglyphClrL = colorAnaglyphLftClrMtx;
		activeAnaglyphClrR = colorAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 3)
	{
		activeAnaglyphClrL = halfColorAnaglyphLftClrMtx;
		activeAnaglyphClrR = halfColorAnaglyphRgtClrMtx;
	}
	else
	{
		activeAnaglyphClrL = optimizedAnaglyphLftClrMtx;
		activeAnaglyphClrR = optimizedAnaglyphRgtClrMtx;
	}

	if(activeAnaglyphColor == 5)
	{
		usingGIMPScheme = 1;
	}
	else
	{
		usingGIMPScheme = 0;
	}

	combineRenderFr = combineRender;

	if(combineRender == 1)
	{
		colorCoord = texCoords;
		fragColor = vec4(0.2, 0.0, 0.6, 1.0);
		gl_Position = position;

		activeClrLeft = activeAnaglyphClrL;
		activeClrRight = activeAnaglyphClrR;


		return;
	}
	else
	{
		unmodVerPos = position.xyz;
		fragNormal = normal;

		vec4 translatedPos = translationMtx * position;
		translatedPos = cameraMtx * translatedPos;
		gl_Position = perspMtxCamToClip * translatedPos;
	//	fragColor = color;

		fragColor = vec4(0.2, 0.2, 0.6, 1.0);
		colorCoord = texCoords;
		drawTexFrag = drawTex;
		anaglyphUsed = 0;

		return;
	}



	// Left frustrum is set
	if(perspMtxType == 1)
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 1.0);
		//fragColor = vec4(0.2, 0.0, 0.6, 0.5);
		modifiedAnaglyphClr.x = 0.2;
		modifiedAnaglyphClr.y = 0.2;
		modifiedAnaglyphClr.z = 0.6;

		modifiedAnaglyphClr = activeAnaglyphClrL * modifiedAnaglyphClr;

		fragColor = vec4(modifiedAnaglyphClr.x, modifiedAnaglyphClr.y, modifiedAnaglyphClr.z, 0.5);
		
		textureColorMtx = activeAnaglyphClrL;
		anaglyphUsed = 1;
	}
	// Right frustrum is set
	else if(perspMtxType == 2)
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 0.5);
		//fragColor = vec4(0.2, 0.0, 0.6, 1.0);
		modifiedAnaglyphClr.x = 0.2;
		modifiedAnaglyphClr.y = 0.2;
		modifiedAnaglyphClr.z = 0.6;

		modifiedAnaglyphClr = activeAnaglyphClrR * modifiedAnaglyphClr;

		fragColor = vec4(modifiedAnaglyphClr.x, modifiedAnaglyphClr.y, modifiedAnaglyphClr.z, 0.5);

		textureColorMtx = activeAnaglyphClrR;
		anaglyphUsed = 1;
	}
	else
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 1.0);
		fragColor = vec4(0.2, 0.0, 0.6, 1.0);
		anaglyphUsed = 0;
	}

	colorCoord = texCoords;
	drawTexFrag = drawTex;
}
