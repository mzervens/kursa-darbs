#version 330

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texCoords;

uniform mat4 perspMtxCamToClip;

uniform mat4 translationMtx;

uniform mat4 cameraMtx;



uniform int perspMtxType;

uniform int drawTex;
out vec2 colorCoord;
out int drawTexFrag;

uniform mat4 rotationXMtx;
uniform mat4 rotationYMtx;
uniform mat4 rotationZMtx;



uniform int activeAnaglyphColor;

uniform mat3 trueAnaglyphLftClrMtx;
uniform mat3 trueAnaglyphRgtClrMtx;
		
		
uniform mat3 greyAnaglyphLftClrMtx;
uniform mat3 greyAnaglyphRgtClrMtx;


uniform mat3 colorAnaglyphLftClrMtx;
uniform mat3 colorAnaglyphRgtClrMtx;


uniform mat3 halfColorAnaglyphLftClrMtx;
uniform mat3 halfColorAnaglyphRgtClrMtx;


uniform mat3 optimizedAnaglyphLftClrMtx;
uniform mat3 optimizedAnaglyphRgtClrMtx;

out mat3 textureColorMtx;
out int anaglyphUsed;

smooth out vec4 fragColor;

void main()
{
	vec4 translatedPos = translationMtx * position;
	translatedPos = cameraMtx * translatedPos;
	gl_Position = perspMtxCamToClip * translatedPos;
//	fragColor = color;


	mat3 activeAnaglyphClrL;
	mat3 activeAnaglyphClrR;

	vec3 modifiedAnaglyphClr;

	if(activeAnaglyphColor == 0)
	{
		activeAnaglyphClrL = trueAnaglyphLftClrMtx;
		activeAnaglyphClrR = trueAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 1)
	{
		activeAnaglyphClrL = greyAnaglyphLftClrMtx;
		activeAnaglyphClrR = greyAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 2)
	{
		activeAnaglyphClrL = colorAnaglyphLftClrMtx;
		activeAnaglyphClrR = colorAnaglyphRgtClrMtx;
	}
	else if(activeAnaglyphColor == 3)
	{
		activeAnaglyphClrL = halfColorAnaglyphLftClrMtx;
		activeAnaglyphClrR = halfColorAnaglyphRgtClrMtx;
	}
	else
	{
		activeAnaglyphClrL = optimizedAnaglyphLftClrMtx;
		activeAnaglyphClrR = optimizedAnaglyphRgtClrMtx;
	}

	// Left frustrum is set
	if(perspMtxType == 1)
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 1.0);
		//fragColor = vec4(0.2, 0.0, 0.6, 0.5);
		modifiedAnaglyphClr.x = 0.2;
		modifiedAnaglyphClr.y = 0.2;
		modifiedAnaglyphClr.z = 0.6;

		modifiedAnaglyphClr = activeAnaglyphClrL * modifiedAnaglyphClr;

		fragColor = vec4(modifiedAnaglyphClr.x, modifiedAnaglyphClr.y, modifiedAnaglyphClr.z, 1.0);
		
		textureColorMtx = activeAnaglyphClrL;
		anaglyphUsed = 1;
	}
	// Right frustrum is set
	else if(perspMtxType == 2)
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 0.5);
		//fragColor = vec4(0.2, 0.0, 0.6, 1.0);
		modifiedAnaglyphClr.x = 0.2;
		modifiedAnaglyphClr.y = 0.2;
		modifiedAnaglyphClr.z = 0.6;

		modifiedAnaglyphClr = activeAnaglyphClrR * modifiedAnaglyphClr;

		fragColor = vec4(modifiedAnaglyphClr.x, modifiedAnaglyphClr.y, modifiedAnaglyphClr.z, 1.0);

		textureColorMtx = activeAnaglyphClrR;
		anaglyphUsed = 1;
	}
	else
	{
		//fragColor = vec4(0.2, 0.2, 0.6, 1.0);
		fragColor = vec4(0.2, 0.0, 0.6, 1.0);
		anaglyphUsed = 0;
	}

	colorCoord = texCoords;
	drawTexFrag = drawTex;
}
