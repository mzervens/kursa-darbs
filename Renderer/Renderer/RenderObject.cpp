#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <exception>


using namespace std;


class RenderObject
{
	template <typename T>
	void addData(vector<T> data, vector<T> * thisDta)
	{
		for (int i = 0; i < data.size(); i++)
		{
			thisDta->push_back(data[i]);
		}
	
	}

public:
	unsigned int vertexBufferHandle;
	unsigned int indexBufferHandle;
	
	vector<float> data;
	vector<unsigned int> positions;

	int dataOffset;

	int normalsOffset;

	int textureCoordOffset;

	void addVertexData(vector<float> data)
	{
		addData(data, &this->data);
	}

	void addPositionData(vector<unsigned int> data)
	{
		addData(data, &this->positions);
	}




};