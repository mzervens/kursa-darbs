#version 330

in vec4 fragColor;

out vec4 outputColor;

layout(location = 0) out vec4 color1;
layout(location = 1) out vec4 color2;

in vec2 colorCoord;
in flat int drawTexFrag;
uniform sampler2D colorTexture;

uniform sampler2D anaglyphLeft;
uniform sampler2D anaglyphRight;

in mat3 textureColorMtx;
in flat int anaglyphUsed;

in flat int combineRenderFr;


in mat3 activeClrLeft;
in mat3 activeClrRight;



// Lighting
uniform mat4 cameraMtx;
in vec3 unmodVerPos;
in vec3 fragNormal;
uniform vec3 lightPosition;
uniform vec3 lightColor;

in flat int usingGIMPScheme;

void main()
{
	if(combineRenderFr == 1)
	{
		vec4 col3 = texture(colorTexture, colorCoord);
		
		
		vec4 col4 = texture(anaglyphLeft, colorCoord);

		if(usingGIMPScheme == 1)
		{
			vec3 out1 = vec3(1, col3.yz);
			vec3 out2 = vec3(col4.x, 1, 1);

			out1 = out1 * out2;
			color2 = vec4(out1.xyz, 1.0);
			color1 = color2;

			return;
		}


		vec3 out1 = activeClrLeft * col3.xyz;
		vec3 out2 = activeClrRight * col4.xyz;

		out1 = out1 + out2;


		color2 = vec4(out1.xyz, 1.0);
		color1 = color2;
		return;



		
		color2 = col3;
		color1 = col3;
		return;


	/*	vec4 col1 = texture(anaglyphLeft, UV);//colorCoord);
		vec4 col2 = texture(anaglyphRight, UV);//colorCoord);

		mat3 col1Mat = mat3(1.0);
		mat3 col2Mat = mat3(1.0);

		col1Mat[0].x = 0.0;
		col1Mat[1].x = 0.7;
		col1Mat[2].x = 0.3;

		col1Mat[1].y = 0.0;
		col1Mat[2].z = 0.0;

		col2Mat[0].x = 0.0;


		vec3 out1 = col1Mat * col1.xyz;
		vec3 out2 = col2Mat * col2.xyz;

		out1 = out1 + out2;

		outputColor = vec4(out1.xyz, 1.0);
		


		outputColor = fragColor;


		color1 = fragColor;
		//color2 = fragColor;

		return;
		*/
	}
	else
	{
		if(drawTexFrag == 1)
		{
		
			color1 = texture(colorTexture, colorCoord);
		}
		else
		{

		color1 = fragColor;
		//color2 = fragColor;
		}

		mat3 normalMat = transpose(inverse(mat3(cameraMtx))); 
		vec3 norm = normalize(normalMat * fragNormal);

		vec3 fragPos = vec3(cameraMtx * vec4(unmodVerPos, 1));

		vec3 lightVector = lightPosition - fragPos;

		float brightness = dot(norm, lightVector) / (length(lightVector) * length(norm));
		brightness = clamp(brightness, 0, 1);

		color1 = vec4(brightness * lightColor * color1.rgb, color1.a);


		return;
	}


	if(drawTexFrag == 1)
	{
		
		outputColor = texture(colorTexture, colorCoord);
		
		if(anaglyphUsed == 1)
		{
			vec3 modClr;

			// Applies anaglyph filter to the texture
			modClr.x = outputColor.x;
			modClr.y = outputColor.y;
			modClr.z = outputColor.z;

			modClr = textureColorMtx * modClr;

			outputColor.x = modClr.x;
			outputColor.y = modClr.y;
			outputColor.z = modClr.z;
		}

	}
	else
	{
		outputColor = fragColor;
	}
}