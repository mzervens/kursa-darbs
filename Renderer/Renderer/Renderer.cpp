#pragma once

#include <glload/gl_3_3.h>
#include <glload/gl_load.hpp>
#include <GL/freeglut.h>

#include <glutil/Shader.h>

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <exception>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glimg\glimg.h>

#include "RenderObject.cpp"
#include "Camera.cpp"
#include "Texture.cpp"
#include "AnaglyphColors.cpp"
#include "Light.cpp"
#include <fstream>



using namespace std;

enum perspMtxType
{
	PERSPECTIVE_PROJ = 0,
	ANAGLYPH_LEFT = 1,
	ANAGLYPH_RIGHT = 2,
	
};



class Renderer;

static Renderer * instance;

class Renderer
{
	Camera camera;

	GLuint cameraMtxUnif;
	glm::mat4 cameraMtx;

	vector<GLuint> shaders;
	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint vertexArrayObject;
	GLuint shaderProgram;
	GLuint perspectiveMtxUnif;

	GLuint translationMtxUnif;
	GLuint scalingMtxUnif;
	GLuint rotationXMtxUnif;
	GLuint rotationYMtxUnif;
	GLuint rotationZMtxUnif;

	GLbitfield clearOptions;

	RenderObject scene;
public:
	RenderObject house;
	RenderObject fence;
	RenderObject treeBark;
	RenderObject treeLeaves;
	RenderObject torus;
	RenderObject knotTorus;
	GLuint tmpHandle;

private:
	glm::mat4 perspectiveMatrix;
	glm::mat4 translationMatrix;
	glm::mat4 rotationXMatrix;
	glm::mat4 rotationYMatrix;
	glm::mat4 rotationZMatrix;

	glm::mat4 leftFrustrumMtx;
	glm::mat4 rightFrustrumMtx;

	glm::mat4 modelRotationMtx;

	Texture floorTex;
	Texture teapotTex;
	Texture concreteTex;
	Texture coolTex;
	Texture emptyTex;
	Texture emptyTex1;
	Texture brickTex;
	Texture brickTex2;
	Texture woodTex;
	Texture barkTex;
	Texture leafTex;
	Texture FXAATex;
	Texture FXAATex2;

	GLuint FXAAShader;
	GLuint FXAATextureSizeInv;

	Light lighting;

	GLuint frameBufferObject;
	GLuint renderedTextureLeft;
	GLuint renderedTextureRight;

	GLuint depthRenderBuffer;

	GLuint combineUnif;
	int combineUsage;

	GLuint quadBuffer;
	GLuint quadBufferTex;

public:
	AnaglyphColors anaglyphColors;
	bool useFXAA;
private:
	GLuint activeAnaglyphColorUnif;

	GLuint trueAnaglyphLUnif;
	GLuint trueAnaglyphRUnif;

	GLuint greyAnaglyphLUnif;
	GLuint greyAnaglyphRUnif;

	GLuint colorAnaglyphLUnif;
	GLuint colorAnaglyphRUnif;

	GLuint halfColorAnaglyphLUnif;
	GLuint halfColorAnaglyphRUnif;

	GLuint optimizedAnaglyphLUnif;
	GLuint optimizedAnaglyphRUnif;

	GLuint textureUnif;
	int textureUnit;

	GLuint texToScrShader;

	int drawTexture;
	GLuint drawTexFlagUnif;

	float appliedScaleX;
	float appliedScaleY;
	float appliedScaleZ;

	float eyeSeperation;
	float convergenceDist;

	int currPerspMtxType;
	GLuint currPerspMtxUnif;

	float fov;
	float clippingNear;
	float clippingFar;

	float frustrumScaleDeg;
	float frustrumScale;

	bool depthBuffering;

	float aspectRatio;

	float torusRotation;
	bool pozTorRot;

public:
	float xAngle;
	float yAngle;
	float zAngle;


private:

	void setInstance()
	{
		instance = this;
	}

	void initShaders(vector<pair<GLenum, string>> shaders)
	{
		fstream f;

		for (int i = 0; i < shaders.size(); i++)
		{
			string data;

			f.open(shaders[i].second, ios::in);
			f.seekg(0, ios::end);

			data.resize(f.tellg());
			f.seekg(0);
			f.read(&data[0], data.size());

			try
			{
				this->shaders.push_back(glutil::CompileShader(shaders[i].first, data.c_str()));
			}
			catch (exception &e)
			{
				cout << e.what() << "\n";
				throw;
			}


			f.close();
		}

	}

	void destroyShaders()
	{
		for (int i = 0; i < shaders.size(); i++)
		{
			glDeleteShader(shaders[i]);
		}

		shaders.clear();

	}

	void initTextureToScreen()
	{
		try
		{
			texToScrShader = glutil::LinkProgram(shaders);
		}
		catch (exception &e)
		{
			cout << e.what() << "\n";
			throw;
		}

		destroyShaders();

		glUseProgram(texToScrShader);

		GLuint texUnif = glGetUniformLocation(texToScrShader, "colorTexture");
		glUniform1i(texUnif, 0);

		glUseProgram(0);
	}

	void initFXAA()
	{
		try
		{
			FXAAShader = glutil::LinkProgram(shaders);
		}
		catch (exception &e)
		{
			cout << e.what() << "\n";
			throw;
		}

		destroyShaders();

		glUseProgram(FXAAShader);

		GLuint texUnif = glGetUniformLocation(FXAAShader, "colorTexture");
		glUniform1i(texUnif, 0);

		glm::vec2 vec;
		vec.x = 1.0f / FXAATex.width;
		vec.y = 1.0f / FXAATex.height;

		FXAATextureSizeInv = glGetUniformLocation(FXAAShader, "textureSizeInv");
		glUniform2fv(FXAATextureSizeInv, 1, glm::value_ptr(vec));

		glUseProgram(0);

	}

	void initShaderProgram()
	{
		try
		{
			shaderProgram = glutil::LinkProgram(shaders);
		}
		catch (exception &e)
		{
			cout << e.what() << "\n";
			throw;
		}

		destroyShaders();

		glUseProgram(shaderProgram);

		perspectiveMtxUnif = glGetUniformLocation(shaderProgram, "perspMtxCamToClip");
		glUniformMatrix4fv(perspectiveMtxUnif, 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));

		translationMtxUnif = glGetUniformLocation(shaderProgram, "translationMtx");
		glUniformMatrix4fv(translationMtxUnif, 1, GL_FALSE, glm::value_ptr(translationMatrix));


		cameraMtxUnif = glGetUniformLocation(shaderProgram, "cameraMtx");
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));

		currPerspMtxUnif = glGetUniformLocation(shaderProgram, "perspMtxType");
		glUniform1i(currPerspMtxUnif, currPerspMtxType);

		textureUnif = glGetUniformLocation(shaderProgram, "colorTexture");
		glUniform1i(textureUnif, textureUnit);

		GLuint textureLeft = glGetUniformLocation(shaderProgram, "anaglyphLeft");
		glUniform1i(textureLeft, 1);

		GLuint textureRight = glGetUniformLocation(shaderProgram, "anaglyphRight");
		glUniform1i(textureRight, 2);

		combineUnif = glGetUniformLocation(shaderProgram, "combineRender");
		glUniform1i(combineUnif, combineUsage);


		trueAnaglyphLUnif = glGetUniformLocation(shaderProgram, "trueAnaglyphLftClrMtx");
		glUniformMatrix3fv(trueAnaglyphLUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.trueAnaglyphLeft));

		trueAnaglyphRUnif = glGetUniformLocation(shaderProgram, "trueAnaglyphRgtClrMtx");
		glUniformMatrix3fv(trueAnaglyphRUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.trueAnaglyphRight));

		greyAnaglyphLUnif = glGetUniformLocation(shaderProgram, "greyAnaglyphLftClrMtx");
		glUniformMatrix3fv(greyAnaglyphLUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.greyAnaglyphLeft));

		greyAnaglyphRUnif = glGetUniformLocation(shaderProgram, "greyAnaglyphRgtClrMtx");
		glUniformMatrix3fv(greyAnaglyphRUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.greyAnaglyphRight));

		colorAnaglyphLUnif = glGetUniformLocation(shaderProgram, "colorAnaglyphLftClrMtx");
		glUniformMatrix3fv(colorAnaglyphLUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.colorAnaglyphLeft));

		colorAnaglyphRUnif = glGetUniformLocation(shaderProgram, "colorAnaglyphRgtClrMtx");
		glUniformMatrix3fv(colorAnaglyphRUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.colorAnaglyphRight));

		halfColorAnaglyphLUnif = glGetUniformLocation(shaderProgram, "halfColorAnaglyphLftClrMtx");
		glUniformMatrix3fv(halfColorAnaglyphLUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.halfColorAnaglyphLeft));

		halfColorAnaglyphRUnif = glGetUniformLocation(shaderProgram, "halfColorAnaglyphRgtClrMtx");
		glUniformMatrix3fv(halfColorAnaglyphRUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.halfColorAnaglyphRight));

		optimizedAnaglyphLUnif = glGetUniformLocation(shaderProgram, "optimizedAnaglyphLftClrMtx");
		glUniformMatrix3fv(optimizedAnaglyphLUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.optimizedAnaglyphLeft));

		optimizedAnaglyphRUnif = glGetUniformLocation(shaderProgram, "optimizedAnaglyphRgtClrMtx");
		glUniformMatrix3fv(optimizedAnaglyphRUnif, 1, GL_FALSE, glm::value_ptr(anaglyphColors.optimizedAnaglyphRight));


		activeAnaglyphColorUnif = glGetUniformLocation(shaderProgram, "activeAnaglyphColor");
		glUniform1i(activeAnaglyphColorUnif, anaglyphColors.currActColorType);


		drawTexFlagUnif = glGetUniformLocation(shaderProgram, "drawTex");
		glUniform1i(drawTexFlagUnif, drawTexture);


		lighting.lightColUnif = glGetUniformLocation(shaderProgram, "lightColor");
		glUniform3fv(lighting.lightColUnif, 1, glm::value_ptr(lighting.color));


		lighting.lightPosUnif = glGetUniformLocation(shaderProgram, "lightPosition");
		glUniform3fv(lighting.lightPosUnif, 1, glm::value_ptr(lighting.position));

		glUseProgram(0);

	}

	void initQuadBuffer()
	{

		const float quadPositions[] = 
		{ 
			1.0, 1.0, 0.0,
			-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0,
			1.0, 1.0, 0.0, 

			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0
		};

		glGenBuffers(1, &quadBuffer);

		glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadPositions), quadPositions, GL_STATIC_DRAW);


		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}

	void initVertexBuffer(vector<float> dta = vector<float>())
	{
		if (dta.size() == 0)
		{
			// Creates OpenGL buffer and sets its handle to vertexBuffer
			glGenBuffers(1, &vertexBuffer);

			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)* scene.data.size(), &scene.data.front(), GL_STATIC_DRAW);


			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		else
		{
			glGenBuffers(1, &tmpHandle);

			glBindBuffer(GL_ARRAY_BUFFER, tmpHandle);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)* dta.size(), &dta.front(), GL_STATIC_DRAW);


			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
	}

	void initIndexBuffer(vector<unsigned int> dta = vector<unsigned int>())
	{
		if (dta.size() == 0)
		{
			glGenBuffers(1, &indexBuffer);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* scene.positions.size(), &scene.positions.front(), GL_STATIC_DRAW);


			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}
		else
		{
			glGenBuffers(1, &tmpHandle);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tmpHandle);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)* dta.size(), &dta.front(), GL_STATIC_DRAW);


			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		}
	}

	void initVAO()
	{
		glGenVertexArrays(1, &vertexArrayObject);
		glBindVertexArray(vertexArrayObject);
	}

	// Check https://msdn.microsoft.com/en-us/library/dd373537(v=VS.85).aspx
	void initAnaglyphProjectionMtx(float zNear, float zFar, float top, float bottom, float left, float right, bool leftMtx)
	{
		float A = (right + left) / (right - left);
		float B = (top + bottom) / (top - bottom);
		float C = -(zFar + zNear) / (zFar - zNear);
		float D = (-(2 * zFar * zNear)) / (zFar - zNear);

		if (leftMtx)
		{
			leftFrustrumMtx[0].x = (2 * zNear) / (right - left);
			leftFrustrumMtx[1].y = (2 * zNear) / (top - bottom);

			leftFrustrumMtx[2].x = A;
			leftFrustrumMtx[2].y = B;
			leftFrustrumMtx[2].z = C;
			leftFrustrumMtx[2].w = -1.0f;

			leftFrustrumMtx[3].z = D;

			leftFrustrumMtx[3].w = 0.0f;
		}
		else
		{
			rightFrustrumMtx[0].x = (2 * zNear) / (right - left);
			rightFrustrumMtx[1].y = (2 * zNear) / (top - bottom);

			rightFrustrumMtx[2].x = A;
			rightFrustrumMtx[2].y = B;
			rightFrustrumMtx[2].z = C;
			rightFrustrumMtx[2].w = -1.0f;

			rightFrustrumMtx[3].z = D;

			rightFrustrumMtx[3].w = 0.0f;

		}
	}

	void initAnaglyphFrustrums(float fovDeg, float zNear, float zFar)
	{
		float fov = fovDeg * 3.14159f / 180.0f;
		float bottom, top, left, right;


		top = zNear * tan(fov / 2);
		bottom = -top;

		float a = aspectRatio * tan(fov / 2) * convergenceDist;
		float b = a - eyeSeperation / 2;
		float c = a + eyeSeperation / 2;

		left = -b * zNear / convergenceDist;
		right = c * zNear / convergenceDist;

		// Left matrix
		initAnaglyphProjectionMtx(zNear, zFar, top, bottom, left, right, true);

		left = -c * zNear / convergenceDist;
		right = b * zNear / convergenceDist;

		// Right matrix
		initAnaglyphProjectionMtx(zNear, zFar, top, bottom, left, right, false);
	}

	void setPerspectiveMtx(glm::mat4 mtx, perspMtxType t)
	{
		glUseProgram(shaderProgram);

		glUniform1i(currPerspMtxUnif, t);

		glUniformMatrix4fv(perspectiveMtxUnif, 1, GL_FALSE, glm::value_ptr(mtx));

		//glUseProgram(0);
	}


	/**
	 */
	void initPerspectiveMtx(float fovDeg, float aspectRatio, float zNear, float zFar)
	{
		frustrumScaleDeg = fovDeg;
		frustrumScale = calcFrustumScale(fovDeg);

		perspectiveMatrix[0].x = frustrumScale / aspectRatio;
		perspectiveMatrix[1].y = frustrumScale;
		perspectiveMatrix[2].z = (zNear + zFar) / (zNear - zFar);
		perspectiveMatrix[2].w = -1.0f;
		perspectiveMatrix[3].z = (2 * zFar * zNear) / (zNear - zFar);

	}

	float degreeToRads(float deg)
	{
		float degToRad = 3.14159f * 2.0f / 360.0f;
		return deg * degToRad;
	}

	float calcFrustumScale(float fovDeg)
	{
		float fovRad = degreeToRads(fovDeg);
		return 1.0f / tan(fovRad / 2.0f);
	}

	void initTranslationMtx()
	{
		translationMatrix[0].x = 1.0f;
		translationMatrix[1].y = 1.0f;
		translationMatrix[2].z = 1.0f;
		translationMatrix[3].w = 1.0f;
	}

	void initRotationMtx()
	{
		rotationXMatrix = orientateAroundX(0.0f);
		rotationYMatrix = orientateAroundY(0.0f);

		glUseProgram(shaderProgram);

		rotationXMtxUnif = glGetUniformLocation(shaderProgram, "rotationXMtx");
		rotationYMtxUnif = glGetUniformLocation(shaderProgram, "rotationYMtx");
		rotationZMtxUnif = glGetUniformLocation(shaderProgram, "rotationZMtx");

		glUniformMatrix4fv(rotationXMtxUnif, 1, GL_FALSE, glm::value_ptr(rotationXMatrix));
		glUniformMatrix4fv(rotationYMtxUnif, 1, GL_FALSE, glm::value_ptr(rotationYMatrix));
		glUniformMatrix4fv(rotationZMtxUnif, 1, GL_FALSE, glm::value_ptr(rotationZMatrix));

		glUseProgram(0);

	}

	void enableDepthBuffering()
	{
		depthBuffering = true;
		clearOptions = clearOptions | GL_DEPTH_BUFFER_BIT;

		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		//	glDepthFunc(GL_LEQUAL);
		glDepthFunc(GL_LESS);
		glDepthRange(0.0f, 1.0f);

	}

	void enableFaceCulling()
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		//glFrontFace(GL_CW);
		glFrontFace(GL_CCW);
	}


public:

	void updateActiveAnaglyphClr()
	{
		glUseProgram(shaderProgram);

		activeAnaglyphColorUnif = glGetUniformLocation(shaderProgram, "activeAnaglyphColor");
		glUniform1i(activeAnaglyphColorUnif, anaglyphColors.currActColorType);

		glUseProgram(0);
	}


	Renderer(int majorVersion, int minorVersion, bool defaultShaders, RenderObject data[])
	{

		xAngle = yAngle = zAngle = 0.0f;
		aspectRatio = 1.77777f;
		depthBuffering = false;
		setInstance();
		scene = data[0];
		house = data[1];
		fence = data[2];
		treeBark = data[3];
		treeLeaves = data[4];
		torus = data[5];
		knotTorus = data[6];
		clearOptions = GL_COLOR_BUFFER_BIT;

		glutInitContextVersion(majorVersion, minorVersion);
		glload::LoadFunctions();


		if (defaultShaders)
		{
			initShaders
				(
				vector<pair<GLenum, string>>
			{
				pair<GLenum, string>{GL_FRAGMENT_SHADER, "fragment.frag"},
					pair<GLenum, string>{GL_VERTEX_SHADER, "vertex.vert"}
			}
			);
		}

		textureUnit = 0;
		drawTexture = 0;
		floorTex = textureFactory("\\textures\\bronze.png");
		teapotTex = textureFactory("\\textures\\teapot1.png");
		concreteTex = textureFactory("\\textures\\tile.png");
		emptyTex = Texture(getWidth(), getHeight());
		emptyTex1 = Texture(getWidth(), getHeight());
		brickTex = textureFactory("\\textures\\seemlessBrick.png");
		brickTex2 = textureFactory("\\textures\\concrete.png");
		woodTex = textureFactory("\\textures\\wood.png");
		leafTex = textureFactory("\\textures\\leafPattern.png");
		barkTex = textureFactory("\\textures\\bark.png");
		FXAATex = Texture(getWidth(), getHeight(), GL_NEAREST);
		FXAATex2 = Texture(getWidth(), getHeight());
		coolTex = textureFactory("\\textures\\CoolTexture.png");

		useFXAA = false;

		lighting = Light(glm::vec3(0.0f, 500.0f, 0.0f));

		combineUsage = 0;

		setScales();

		anaglyphColors = AnaglyphColors();
		anaglyphColors.setActiveColorAnaglyph();

		camera.moveToPosition(glm::vec3(0.0f, 0.0f, -7425.0f));
		camera.setRotation(Quaternion(0.0930795f, -0.0581809f, 0.00544862f, 0.993942f));
		cameraMtx = camera.getViewMtx();

		initPerspectiveMtx(35.0f, aspectRatio, 1.0f, 20000.0f);
		initTranslationMtx();

		eyeSeperation = 212.67f;
		//eyeSeperation = 0.1f;
		convergenceDist = 6400.0f;
	//	convergenceDist = 4000.0f;
	//	eyeSeperation = 133.0f;

		this->fov = 45.0f;
		this->clippingNear = 0.1f;
		this->clippingFar = 20000.0f;

		initAnaglyphFrustrums(fov, clippingNear, clippingFar);

		currPerspMtxType = perspMtxType::PERSPECTIVE_PROJ;

		initShaderProgram();

		initShaders
		(
			vector<pair<GLenum, string>>
			{
				pair<GLenum, string>{GL_FRAGMENT_SHADER, "fxaa.frag"},
				pair<GLenum, string>{GL_VERTEX_SHADER, "fxaa.vert"}
			}
		);

		initFXAA();

		initShaders
		(
			vector<pair<GLenum, string>>
			{
				pair<GLenum, string>{GL_FRAGMENT_SHADER, "textureToScreen.frag"},
				pair<GLenum, string>{GL_VERTEX_SHADER, "textureToScreen.vert"}
			}
		);

		initTextureToScreen();

		initRotationMtx();

		initVAO();

		initVertexBuffer();
		initIndexBuffer();
		initQuadBuffer();

		initRenderObjBuffers(house);

		initRenderObjBuffers(fence);

		initRenderObjBuffers(treeBark);

		initRenderObjBuffers(treeLeaves);

		initRenderObjBuffers(torus);

		initRenderObjBuffers(knotTorus);
		torusRotation = 0.0f;
		pozTorRot = true;
		

		initRenderToTexture();

		enableDepthBuffering();
		enableFaceCulling();

		glutDisplayFunc(renderCallback);
	}

	void initRenderObjBuffers(RenderObject &obj)
	{
		initVertexBuffer(obj.data);
		obj.vertexBufferHandle = tmpHandle;

		initIndexBuffer(obj.positions);
		obj.indexBufferHandle = tmpHandle;

	}

	Texture textureFactory(string relativePath)
	{
		Texture tex;

		return tex.loadRelative(relativePath);
	}



	void adjustFrustrumScale(float frustrumScaleDeg)
	{
		this->frustrumScaleDeg += frustrumScaleDeg;
		frustrumScale = calcFrustumScale(this->frustrumScaleDeg);

		glUseProgram(shaderProgram);

		perspectiveMatrix[0].x = this->frustrumScale / aspectRatio;
		perspectiveMatrix[1].y = this->frustrumScale;
		glUniformMatrix4fv(perspectiveMtxUnif, 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));

		glUseProgram(0);

	}

	void setAspectRatio(float aspectRatio)
	{
		this->aspectRatio = aspectRatio;

		glUseProgram(shaderProgram);

		perspectiveMatrix[0].x = frustrumScale / aspectRatio;
		glUniformMatrix4fv(perspectiveMtxUnif, 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));

		glUseProgram(0);
	}

	void setScales(float x = 1.0f, float y = 1.0f, float z = 1.0f)
	{
		appliedScaleX = x;
		appliedScaleY = y;
		appliedScaleZ = z;

		return;
	}

	void rotateModel(float angle, rotationType axis)
	{

		modelRotationMtx = camera.getRotationMtx(angle, axis);
	}

	void translate(float x, float y, float z, bool applyScale, bool applyRotation = false)
	{
		/*	translationMatrix[3].x = x;
			translationMatrix[3].y = y;
			translationMatrix[3].z = z;

			if (applyScale)
			{
			translationMatrix[0].x = appliedScaleX;
			translationMatrix[1].y = appliedScaleY;

			}
			else
			{
			translationMatrix[0].x = 1.0f;
			translationMatrix[1].y = 1.0f;
			}
			*/

		cameraMtx = camera.getViewMtx();

		glm::mat4 trans = glm::mat4();

		trans[3].x = x;
		trans[3].y = y;
		trans[3].z = z;

		/*	cameraMtx[3].x += x;
			cameraMtx[3].y += y;
			cameraMtx[3].z += z;
			*/

		if (applyScale && applyRotation == false)
		{
			glm::mat4 scale = glm::mat4();
			scale[0].x = appliedScaleX;
			scale[1].y = appliedScaleY;
			scale[2].z = appliedScaleZ;

			cameraMtx = cameraMtx * trans * scale;
			//		cameraMtx = cameraMtx * translationMatrix;

			//cameraMtx[0].x += appliedScaleX;
			//cameraMtx[1].y += appliedScaleY;
		}
		else if (applyScale && applyRotation)
		{
			glm::mat4 scale = glm::mat4();
			scale[0].x = appliedScaleX;
			scale[1].y = appliedScaleY;
			scale[2].z = appliedScaleZ;

			cameraMtx = cameraMtx * modelRotationMtx * trans * scale;

		}
		else if (applyScale == false && applyRotation)
		{
			cameraMtx = cameraMtx * modelRotationMtx * trans;
		}
		else
		{
			cameraMtx = cameraMtx * trans;
		}


		glUseProgram(shaderProgram);

		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));

		//	glUseProgram(0)


		//	glUseProgram(shaderProgram);

		//	glUniformMatrix4fv(translationMtxUnif, 1, GL_FALSE, glm::value_ptr(translationMatrix));

		//	glUseProgram(0);

	}

	void updateXOrientation()
	{
		glm::mat4 matrix = orientateAroundX(0.0f);
		rotationXMatrix = matrix;

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(rotationXMtxUnif, 1, GL_FALSE, glm::value_ptr(rotationXMatrix));
		glUseProgram(0);
	}

	glm::mat4 orientateAroundX(float angleDeg)
	{
		angleDeg += xAngle;
		xAngle = angleDeg;

		glm::mat4 matrix;

		float angleRads = degreeToRads(angleDeg);
		matrix[0].x = 1.0f;
		matrix[1].y = cos(angleRads);
		matrix[1].z = sin(angleRads);
		matrix[2].y = -sin(angleRads);
		matrix[2].z = cos(angleRads);
		matrix[3].w = 1.0f;

		return matrix;
	}

	void updateYOrientation()
	{
		glm::mat4 matrix = orientateAroundY(0.0f);
		rotationYMatrix = matrix;

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(rotationYMtxUnif, 1, GL_FALSE, glm::value_ptr(rotationYMatrix));
		glUseProgram(0);
	}

	glm::mat4 orientateAroundY(float angleDeg)
	{
		angleDeg += yAngle;
		yAngle = angleDeg;

		glm::mat4 matrix;

		float angleRads = degreeToRads(angleDeg);
		matrix[0].x = cos(angleRads);
		matrix[0].z = -sin(angleRads);
		matrix[1].y = 1.0f;
		matrix[2].x = sin(angleRads);
		matrix[2].z = cos(angleRads);
		matrix[3].w = 1.0f;

		return matrix;

	}

	void camFwdCallback()
	{
		camera.moveForward();

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);
	}

	void camBwdCallback()
	{
		camera.moveBackward();

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);
	}

	void camLeftCallback()
	{
		camera.moveLeft();

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);
	}

	void camRightCallback()
	{
		camera.moveRight();

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);
	}


	void camYawCallback(float angle)
	{
		camera.yaw(angle);

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);
	}

	void camPitchCallback(float angle)
	{
		camera.pitch(angle);

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);

	}

	void orientateCamera(float yaw, float pitch)
	{
		camera.orientate(yaw, pitch);

		cameraMtx = camera.getViewMtx();

		glUseProgram(shaderProgram);
		glUniformMatrix4fv(cameraMtxUnif, 1, GL_FALSE, glm::value_ptr(cameraMtx));
		glUseProgram(0);

	}




	Renderer * getReference()
	{
		return this;
	}

	void startLoop()
	{
		glutMainLoop();
	}

	static void renderCallback()
	{
		instance->render();
	}

	void renderTMP()
	{
		// (1) Tiek uzst�d�tas noklus�t�s pikse�u un citu parametru v�rt�bas, kas p�rraksta p�ri iepriek��jo funkcijas
		// render�to att�lu
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);
		glClear(clearOptions);
		//


		// (2) Aktiv� �not�jprogrammu, kura veiks t�l�k�s darb�bas (uz GPU) p�c z�m��anas funkciju izsaukumiem �aj� funkcij�
		glUseProgram(shaderProgram);
		//
        

		for (int i = 0; i < 2; i++)
		{
			// (2.5)
			float sepOffset;

			if (i == 0)
			{
				setPerspectiveMtx(leftFrustrumMtx, perspMtxType::ANAGLYPH_LEFT);
				sepOffset = eyeSeperation / 2;
			}
			else
			{
				glClear(GL_DEPTH_BUFFER_BIT);
				glEnable(GL_BLEND);
				glBlendFunc(GL_ONE, GL_ONE);

				setPerspectiveMtx(rightFrustrumMtx, perspMtxType::ANAGLYPH_RIGHT);
				sepOffset = -eyeSeperation / 2;
			}
			//

			// (3) Aktiv� buferus, kuri satur render�jamo objektu datus (virsotnes, virsmas norm�les, tekst�ru koordin�tas u.c.)
			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);

			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));

			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
			//

			// (4) Aktiv� tekst�ru, kuru izmantos render��an�, tiek uzst�d�ta mode�a p�rveides matrica (modelToWorld), kas
			// p�rvietos z�m�tos objektus 1815 vien�bas dzi��k (pa Z asi) no kameras s�kumpunkta, paplet�s objektus par 10000 vien�b�m pa X un Z asi, par 500 vien�b�m pa Y asi,
			// tiek veikts z�m��anas izsaukums, kas z�m�s kvadr�tveida plakni
			glUniform1i(drawTexFlagUnif, 1);
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			floorTex.bind();

			setScales(10000.0f, 500.3f, 10000.0f);
			translate(sepOffset, 0.0f, -1815.0f, true);

			glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
			//
		}

		// (5) Tiek padar�ti neakt�vi izmantotie resursi no OpenGL konteksta,
		// tiek par�d�ts render�tais att�ls lietot�jam, tiek turpin�ts render��anas cikls no jauna
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		glUseProgram(0);

		glutSwapBuffers();

		glutPostRedisplay();
		//
	}

	void renderOLD()
	{

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);
		//	

		glUseProgram(shaderProgram);

		glUniform1i(combineUnif, 1);

		setPerspectiveMtx(leftFrustrumMtx, perspMtxType::ANAGLYPH_LEFT);


		glUniform1i(drawTexFlagUnif, 0);
		floorTex.unbind();

		// Test
		setScales(1600.0f, 3700.0f, 100.0f);
		translate(eyeSeperation / 2, 1200.0f, -6015.0f, true);
		drawConcretePilar();


		setScales(400.0f, 1200.0f, 100.0f);
		translate(eyeSeperation / 2 + 1800, 0.0f, -715.0f, true);
		drawConcretePilar();



		setScales(400.0f, 800.0f, 100.0f);
		translate(eyeSeperation / 2 - 1800, -100.0f, 1815.0f, true);
		drawConcretePilar();


		//	glColorMask(true, false, false, true);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

		// Activates vertex atribute index
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		// Vertex positions
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);


		// Color attributes (currently not used)
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		floorTex.bind();


		
		setScales(10000.0f, 500.3f, 10000.0f);
		translate(eyeSeperation / 2, 0.0f, -1815.0f, true);


		//	printMatrix(translationMatrix);

		//	glColorMask(true, true, false, true);

		glDrawElements(GL_TRIANGLES, 12/*scene.positions.size()*/, GL_UNSIGNED_INT, 0);

	/*	translate(eyeSeperation / 2, -150.0f, -1815.0f, false);
		// Test

		glutSetVertexAttribCoord3(0);
		//	glutSolidTorus(40, 200, 20, 30);
		//	glutWireTorus(40, 200, 20, 30);

		glutSolidTeapot(400);
		glutWireTeapot(400);
		//	glutWireCube(400.0f);

		translate(eyeSeperation / 2 + 650, -440.0f, -1655.0f, false);

		glutSolidTeacup(150);
		glutWireTeacup(150);

		//	glutSolidTorus(40, 200, 20, 30);
		//	glutWireTorus(40, 200, 20, 30);

		translate(eyeSeperation / 2 - 650, 0.0f, -1955.0f, false);

		//	glutSolidTorus(40, 200, 20, 30);
		//	glutWireTorus(40, 200, 20, 30);
		*/

		glClear(GL_DEPTH_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ONE, GL_ONE);
	//	glBlendFunc(GL_ONE, GL_ONE);
	//	glBlendFunc(GL_ONE, GL_ONE);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);





		//glDrawElements(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, 36);
		//glDrawElementsBaseVertex(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0, offset);
	//	glColorMask(false, true, true, true);

		setPerspectiveMtx(rightFrustrumMtx, perspMtxType::ANAGLYPH_RIGHT);



		// Test
		setScales(1600.0f, 3700.0f, 100.0f);
		translate(-eyeSeperation / 2, 1200.0f, -6015.0f, true);
		drawConcretePilar();


		setScales(400.0f, 1200.0f, 100.0f);
		translate(-eyeSeperation / 2 + 1800, 0.0f, -715.0f, true);
		drawConcretePilar();

		setScales(400.0f, 800.0f, 100.0f);
		translate(-eyeSeperation / 2 - 1800, -100.0f, 1815.0f, true);
		drawConcretePilar();

		setScales(10000.0f, 500.3f, 10000.0f);
		translate(-eyeSeperation / 2, 0.0f, -1815.0f, true);
		//	glColorMask(false, true, true, true);

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

		// Activates vertex atribute index
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

		glUniform1i(drawTexFlagUnif, 1);
		floorTex.bind();



		glDrawElements(GL_TRIANGLES, 12/*scene.positions.size()*/, GL_UNSIGNED_INT, 0);

		glUniform1i(drawTexFlagUnif, 0);
		floorTex.unbind();
	/*	translate(-eyeSeperation / 2, -150.0f, -1815.0f, false);
		// Test


		glutSolidTeapot(400);
		glutWireTeapot(400);


		//glutWireCube(400.0f);

		//	glutSolidTorus(40, 200, 20, 30);
		//	glutWireTorus(40, 200, 20, 30);



		translate(-eyeSeperation / 2 + 650, -440.0f, -1655.0f, false);

		glutSolidTeacup(150);
		glutWireTeacup(150);
		//	glutSolidTorus(40, 200, 20, 30);
		//	glutWireTorus(40, 200, 20, 30);


		translate(-eyeSeperation / 2 - 650, 0.0f, -1955.0f, false);
		*/


		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		// Removes strange visual bug 
		//	glColorMask(true, true, true, true);
	//	glColorMask(true, true, true, true);

		glUseProgram(0);

		glutSwapBuffers();


		// If we want to render constantly
		glutPostRedisplay();
	}

	// Applies FXAA on the rendered frame
	void applyFXAA(GLenum colAttach, Texture target, bool window = false)
	{
		renderTextureToScreen(FXAAShader, target);
	}

	void renderTextureToScreen(GLuint shader, Texture texture)
	{
		setWindowAsDrawingTarget();

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);

		glUseProgram(shader);

		glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)(18 * sizeof(float)));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture.textureHandle);

		glDrawArrays(GL_TRIANGLES, 0, 6);


		glUseProgram(0);

	}

	void render()
	{
		renderTorusScene();
		return;

		GLenum renderTargets[] = { GL_COLOR_ATTACHMENT0};
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferObject);
		glDrawBuffers(1, renderTargets);
		glViewport(0, 0, getWidth(), getHeight());
		
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);
		//	

		glUseProgram(shaderProgram);
		glUniform1i(combineUnif, 0);



		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

		// Activates vertex atribute index
		glEnableVertexAttribArray(0);
		//glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		// Vertex positions
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)((scene.normalsOffset) * sizeof(float)));


		setPerspectiveMtx(leftFrustrumMtx, perspMtxType::ANAGLYPH_LEFT);
	//	translate(eyeSeperation / 2, 0.0f, -1815.0f, false);

		glActiveTexture(GL_TEXTURE0);
		floorTex.bind();
		glUniform1i(drawTexFlagUnif, 1);

		setScales(10000.0f, 500.3f, 10000.0f);
		translate(eyeSeperation / 2, 0.0f, -1815.0f, true);


		glDrawElements(GL_TRIANGLES, 12/*scene.positions.size()*/, GL_UNSIGNED_INT, 0);

		floorTex.unbind();
	

		setScales(1600.0f, 2800.0f, 100.0f);
		translate(eyeSeperation / 2, 2200.0f, -6015.0f, true);
		drawConcretePilar();


		setScales(400.0f, 1200.0f, 100.0f);
		translate(eyeSeperation / 2 + 1800, 0.0f, -715.0f, true);
		drawConcretePilar();


		//
		drawConcretePilarWithRoofs(eyeSeperation / 2 - 2800, 1050.0f, -2715.0f);
		drawConcretePilarWithRoofs(eyeSeperation / 2 - 200, 1050.0f, -3415.0f);
		//

		setScales(250.0f, 250.0f, 250.0f);
		translate(eyeSeperation / 2 - 200, -500.0f, 800.0f, true);
		renderTree();


		setScales(400.0f, 800.0f, 100.0f);
		translate(eyeSeperation / 2 - 1800, -100.0f, 1815.0f, true);
		drawConcretePilar();


		setScales(500.0f, 500.0f, 500.0f);
		translate(eyeSeperation / 2 + 1000, 100.0f, -2400.0f, true);
		renderHouse();

		for (int i = 0; i < 15; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(90.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(eyeSeperation / 2 - 2500, -300.0f, 4415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 15; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(90.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(eyeSeperation / 2 + 5400, -300.0f, 4415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 13; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(180.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(eyeSeperation / 2 + 4600, -300.0f, 5415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 13; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(180.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(eyeSeperation / 2 - 4600, -300.0f, 5415.0f + val, true, true);
			renderFence();
		}



		GLenum renderTargets2[] = { GL_COLOR_ATTACHMENT1 };
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferObject);
		glDrawBuffers(1, renderTargets2);
		glViewport(0, 0, getWidth(), getHeight());

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);



		setPerspectiveMtx(rightFrustrumMtx, perspMtxType::ANAGLYPH_RIGHT);

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

		// Activates vertex atribute index
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)((scene.normalsOffset) * sizeof(float)));

		glActiveTexture(GL_TEXTURE0);
		floorTex.bind();
		glUniform1i(drawTexFlagUnif, 1);

		setScales(10000.0f, 500.3f, 10000.0f);
		translate(-eyeSeperation / 2, 0.0f, -1815.0f, true);

		glDrawElements(GL_TRIANGLES, 12/*scene.positions.size()*/, GL_UNSIGNED_INT, 0);

		floorTex.unbind();

		setScales(1600.0f, 2800.0f, 100.0f);
		translate(-eyeSeperation / 2, 2200.0f, -6015.0f, true);
		drawConcretePilar();


		setScales(400.0f, 1200.0f, 100.0f);
		translate(-eyeSeperation / 2 + 1800, 0.0f, -715.0f, true);
		drawConcretePilar();


		//
		drawConcretePilarWithRoofs(-eyeSeperation / 2 - 2800, 1050.0f, -2715.0f);
		drawConcretePilarWithRoofs(-eyeSeperation / 2 - 200, 1050.0f, -3415.0f);
		//drawConcretePilarWithRoofs(-eyeSeperation / 2 - 1800, 550.0f, -715.0f);
		//

		setScales(250.0f, 250.0f, 250.0f);
		translate(-eyeSeperation / 2 - 200, -500.0f, 800.0f, true);
		renderTree();


		setScales(400.0f, 800.0f, 100.0f);
		translate(-eyeSeperation / 2 - 1800, -100.0f, 1815.0f, true);
		drawConcretePilar();


		setScales(500.0f, 500.0f, 500.0f);
		translate(-eyeSeperation / 2 + 1000, 100.0f, -2400.0f, true);


		renderHouse();


		for (int i = 0; i < 15; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(90.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(-eyeSeperation / 2 - 2500, -300.0f, 4415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 15; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(90.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(-eyeSeperation / 2 + 5400, -300.0f, 4415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 13; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(180.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(-eyeSeperation / 2 + 4600, -300.0f, 5415.0f + val, true, true);
			renderFence();
		}

		for (int i = 0; i < 13; i++)
		{
			float offset = -600;
			float val = offset * i;

			rotateModel(180.0f, rotationType::ROTATION_Y);
			setScales(600.0f, 480.0f, 600.0f);
			translate(-eyeSeperation / 2 - 4600, -300.0f, 5415.0f + val, true, true);
			renderFence();
		}


		GLenum renderTargets3[] = { GL_COLOR_ATTACHMENT2 };
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferObject);
		glDrawBuffers(1, renderTargets3);
		glViewport(0, 0, getWidth(), getHeight());

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);
		
		
		/*glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, getWidth(), getHeight()); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		glClearColor(0.3f, 0.6f, 0.1f, 0.5f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);
		*/
		//	

		glUseProgram(shaderProgram);
		glUniform1i(combineUnif, 1);

		glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)(18 * sizeof(float)));



		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, emptyTex.textureHandle);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, emptyTex1.textureHandle);


		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, 6);


		glUseProgram(0);

		if (useFXAA)
		{
			applyFXAA(NULL, FXAATex, true);
		}
		else
		{

			renderTextureToScreen(texToScrShader, FXAATex);
		}

		glutSwapBuffers();


		// If we want to render constantly
		glutPostRedisplay();
	}


	void renderTorusScene()
	{
		float increment = 1.0f;
		float divider = 150.0f;
		
		
		if (pozTorRot)
		{
			torusRotation += increment;
		}
		else
		{
			torusRotation -= increment;
		}

		if ((torusRotation / divider) >= 65.0f)
		{
			pozTorRot = false;
		}
		else if ((torusRotation / divider) <= 0.0)
		{
			pozTorRot = true;
		}

		for (int i = 0; i < 2; i++)
		{

			GLenum renderTargets[] = { GL_COLOR_ATTACHMENT0 + i };
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferObject);
			glDrawBuffers(1, renderTargets);
			glViewport(0, 0, getWidth(), getHeight());

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

			if (depthBuffering)
			{
				glClearDepth(1.0f);
			}

			glClear(clearOptions);

			glUseProgram(shaderProgram);
			glUniform1i(combineUnif, 0);



			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(2);
			glEnableVertexAttribArray(3);

			// Vertex positions
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
			glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)((scene.normalsOffset) * sizeof(float)));

			if (i == 0)
			{
				setPerspectiveMtx(leftFrustrumMtx, perspMtxType::ANAGLYPH_LEFT);
			}
			else
			{
				setPerspectiveMtx(rightFrustrumMtx, perspMtxType::ANAGLYPH_RIGHT);
			}

			glActiveTexture(GL_TEXTURE0);
			floorTex.bind();
			glUniform1i(drawTexFlagUnif, 1);

			setScales(10000.0f, 500.3f, 10000.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2, 0.0f, -1815.0f, true);
			}
			else
			{
				translate(-eyeSeperation / 2, 0.0f, -1815.0f, true);
			}

			glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
			floorTex.unbind();



			rotateModel(55.0f - (torusRotation / divider), rotationType::ROTATION_X);
			setScales(600.0f, 600.0f, 600.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2 - 2500, 1150.0f, -815.0f, true, true);
			}
			else
			{
				translate(-eyeSeperation / 2 - 2500, 1150.0f, -815.0f, true, true);
			}
			renderTorus();

			rotateModel(25.0f - (torusRotation / divider), rotationType::ROTATION_Y);
			setScales(820.0f, 820.0f, 820.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2 + 350, 1200.0f, -3815.0f, true, true);
			}
			else
			{
				translate(-eyeSeperation / 2 + 350, 1200.0f, -3815.0f, true, true);
			}
			renderTorus();


			rotateModel(25.0f - (torusRotation / divider), rotationType::ROTATION_X);
			setScales(700.0f, 700.0f, 700.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2 + 1500, 700.0f, -1315.0f, true, true);
			}
			else
			{
				translate(-eyeSeperation / 2 + 1500, 700.0f, -1315.0f, true, true);
			}
			renderTorus();

			rotateModel(15.0f - (torusRotation / divider), rotationType::ROTATION_Y);
			setScales(500.0f, 500.0f, 500.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2 + 3500, 500.0f, 815.0f, true, true);
			}
			else
			{
				translate(-eyeSeperation / 2 + 3500, 500.0f, 815.0f, true, true);
			}
			renderTorus();


			setScales(450.0f, 450.0f, 450.0f);
			if (i == 0)
			{
				translate(eyeSeperation / 2 - 700, 400.0f, 800.0f, true);
			}
			else
			{
				translate(-eyeSeperation / 2 - 700, 400.0f, 800.0f, true);
			}
			renderTorus(true);


		}
		if (useFXAA)
		{
			GLenum renderTargets3[] = { GL_COLOR_ATTACHMENT2 };
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferObject);
			glDrawBuffers(1, renderTargets3);
			glViewport(0, 0, getWidth(), getHeight());
		}
		else
		{
			setWindowAsDrawingTarget();
		}
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		if (depthBuffering)
		{
			glClearDepth(1.0f);
		}

		glClear(clearOptions);

		glUseProgram(shaderProgram);
		glUniform1i(combineUnif, 1);

		glBindBuffer(GL_ARRAY_BUFFER, quadBuffer);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)((scene.dataOffset) * sizeof(float)));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)(18 * sizeof(float)));



		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, emptyTex.textureHandle);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, emptyTex1.textureHandle);


		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glUseProgram(0);

		if (useFXAA)
		{
			applyFXAA(NULL, FXAATex, true);
		}


		glutSwapBuffers();


		// If we want to render constantly
		glutPostRedisplay();

	}

	

	// SHOULD BE IN WINDOW CLASS
	int getWidth()
	{
		int width = glutGet(GLUT_SCREEN_WIDTH);
		return width;
	}
	int getHeight()
	{
		int height = glutGet(GLUT_SCREEN_HEIGHT);
		return height;
	}
	//


	void initRenderToTexture()
	{

		glGenFramebuffers(1, &frameBufferObject);
		glBindFramebuffer(GL_FRAMEBUFFER, frameBufferObject);

		// The depth buffer

		glGenRenderbuffers(1, &depthRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, getWidth(), getHeight());
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);



		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, emptyTex.textureHandle, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, emptyTex1.textureHandle, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, FXAATex.textureHandle, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, FXAATex2.textureHandle, 0);

		GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

		// Always check that our framebuffer is ok
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			cout << "Framebuffer could not be initialized" << endl;
		}
		else cout << "ok init" << endl;

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

	}

	void setWindowAsDrawingTarget()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, getWidth(), getHeight());
	}

	void drawUnitCube()
	{
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		// Vertex positions
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)((scene.dataOffset + 16) * sizeof(float)));

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)((scene.normalsOffset + 12) * sizeof(float)));


		glDrawArrays(GL_TRIANGLES, 0, 36);

	}

	void drawConcretePilar()
	{
		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		concreteTex.bind();


		drawUnitCube();
		//glDrawElements(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0);
		//glDrawArrays(GL_TRIANGLES, scene.dataOffset, 96);

		//glDrawElementsBaseVertex(GL_TRIANGLES, scene.positions.size(), GL_UNSIGNED_INT, 0, offset);
		//glDrawElementsBaseVertex(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0, 12);
	//	glutSolidCube(1);

		glUniform1i(drawTexFlagUnif, 0);
		concreteTex.unbind();

	}

	void drawConcretePilarWithRoofs(float translateX, float translateY, float translateZ)
	{
		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		brickTex.bind();

		float pilarHeight = 1500.0f;
		float pilarWidthDepth = 200.0f;

		setScales(pilarWidthDepth, pilarHeight, pilarWidthDepth);
		translate(translateX, translateY, translateZ, true);
		//glutSolidCube(1);
		drawUnitCube();

		brickTex.unbind();
		brickTex2.bind();
		setScales(300.0f, 50.0f, 300.0f);
		translate(translateX, translateY + pilarHeight, translateZ, true);
		//glutSolidCube(1);
		drawUnitCube();

		setScales(300.0f, 50.0f, 300.0f);
		translate(translateX, translateY - pilarHeight, translateZ, true);
		//glutSolidCube(1);
		drawUnitCube();


		glUniform1i(drawTexFlagUnif, 0);
		brickTex.unbind();
		brickTex2.bind();

	}

	void renderHouse()
	{
		glBindBuffer(GL_ARRAY_BUFFER, house.vertexBufferHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, house.indexBufferHandle);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);



		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)house.dataOffset);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)house.textureCoordOffset);
		
		// Normals
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)house.normalsOffset);


		glDrawElements(GL_TRIANGLES, house.positions.size(), GL_UNSIGNED_INT, 0);

	}

	void renderFence()
	{
		glBindBuffer(GL_ARRAY_BUFFER, fence.vertexBufferHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, fence.indexBufferHandle);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		woodTex.bind();

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)fence.dataOffset);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)fence.textureCoordOffset);

		// Normals
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)fence.normalsOffset);


		glDrawElements(GL_TRIANGLES, fence.positions.size(), GL_UNSIGNED_INT, 0);


		glUniform1i(drawTexFlagUnif, 0);
		woodTex.unbind();

	}

	void renderTorus(bool knot = false)
	{
		RenderObject tmp;
		if (knot)
		{
			tmp = torus;
			torus = knotTorus;
		}

		glBindBuffer(GL_ARRAY_BUFFER, torus.vertexBufferHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, torus.indexBufferHandle);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		coolTex.bind();

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)torus.dataOffset);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)torus.textureCoordOffset);

		// Normals
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)torus.normalsOffset);


		glDrawElements(GL_TRIANGLES, torus.positions.size(), GL_UNSIGNED_INT, 0);

		coolTex.unbind();
		glUniform1i(drawTexFlagUnif, 0);

		if (knot)
		{
			torus = tmp;
		}

	}


	void renderTree()
	{
		glBindBuffer(GL_ARRAY_BUFFER, treeBark.vertexBufferHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, treeBark.indexBufferHandle);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glUniform1i(drawTexFlagUnif, 1);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		barkTex.bind();

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)treeBark.dataOffset);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)treeBark.textureCoordOffset);

		// Normals
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)treeBark.normalsOffset);


		glDrawElements(GL_TRIANGLES, treeBark.positions.size(), GL_UNSIGNED_INT, 0);
		barkTex.unbind();



		glBindBuffer(GL_ARRAY_BUFFER, treeLeaves.vertexBufferHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, treeLeaves.indexBufferHandle);

		leafTex.bind();

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void *)treeLeaves.dataOffset);

		// Texture coordinates
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)treeLeaves.textureCoordOffset);

		// Normals
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void *)treeLeaves.normalsOffset);


		glDrawElements(GL_TRIANGLES, treeLeaves.positions.size(), GL_UNSIGNED_INT, 0);


		leafTex.unbind();
		glUniform1i(drawTexFlagUnif, 0);
		

	}


	void printMatrix(glm::mat4 m)
	{

		cout << m[0].x << " " << m[1].x << " " << m[2].x << " " << m[3].x << "\n";
		cout << m[0].y << " " << m[1].y << " " << m[2].y << " " << m[3].y << "\n";
		cout << m[0].z << " " << m[1].z << " " << m[2].z << " " << m[3].z << "\n";
		cout << m[0].w << " " << m[1].w << " " << m[2].w << " " << m[3].w << "\n";

		cout << "\n";
	}


	void adjustEyeSeperation(bool increase)
	{
		float percentage = 0.1f;
		float value = eyeSeperation * percentage;

		if (increase)
		{
			if (value == 0.0f)
			{
				eyeSeperation += 0.3f;
			}
			else
			{
				eyeSeperation += value;
			}
		}
		else
		{
			if (value == 0.0f)
			{
				eyeSeperation -= 0.3f;
			}
			else
			{
				eyeSeperation -= value;
			}

			if (eyeSeperation < 0.0f)
			{
				eyeSeperation = 0.0f;
			}
		}

		initAnaglyphFrustrums(fov, clippingNear, clippingFar);

	}

	void adjustConvergenceDistance(bool increase)
	{
		float minDistance = 1.0f;
		float percentage = 0.1f;
		float value = convergenceDist * percentage;

		cout << "epsilon value " << value << "\n";

		if (increase)
		{
			convergenceDist += value;
		}
		else
		{
			convergenceDist -= value;
			if (convergenceDist < 1)
			{
				convergenceDist = 1;
			}
		}

		cout << "convergence dist " << convergenceDist << "\n";

		initAnaglyphFrustrums(fov, clippingNear, clippingFar);

	}

	void lookAtDefault()
	{
		camera.setRotation(Quaternion(0.0930795f, -0.0581809f, 0.00544862f, 0.993942f));

	}

	void changeAnaglyphFrustrums(float aspect)
	{
	//	cout << "aspect " << aspect << endl;
		this->aspectRatio = aspect;
		initAnaglyphFrustrums(fov, clippingNear, clippingFar);
	}


};


