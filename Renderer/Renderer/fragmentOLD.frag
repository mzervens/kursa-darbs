#version 330

in vec4 fragColor;

out vec4 outputColor;

in vec2 colorCoord;
in flat int drawTexFrag;
uniform sampler2D colorTexture;

in mat3 textureColorMtx;
in flat int anaglyphUsed;

void main()
{
	if(drawTexFrag == 1)
	{
		
		outputColor = texture(colorTexture, colorCoord);
		
		if(anaglyphUsed == 1)
		{
			vec3 modClr;

			// Applies anaglyph filter to the texture
			modClr.x = outputColor.x;
			modClr.y = outputColor.y;
			modClr.z = outputColor.z;

			modClr = textureColorMtx * modClr;

			outputColor.x = modClr.x;
			outputColor.y = modClr.y;
			outputColor.z = modClr.z;
			outputColor.a = 1.0;
			
		}

	}
	else
	{
	//	outputColor = fragColor;
	}
}