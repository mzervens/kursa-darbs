#pragma once
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <vector>
#include <string>
#include <glload/gl_3_3.h>
#include <glload/gl_load.hpp>
#include <GL/freeglut.h>
#include "Renderer.cpp"
#include "Window.cpp"

using namespace std;

class Keyboard;

static Keyboard * keyboardInstance;

class Keyboard
{
	Renderer * rendererInst;

public:
	Keyboard(Renderer * instance)
	{
		srand(time(NULL));

		keyboardInstance = this;
		rendererInst = instance;

		glutKeyboardFunc(keyboardCallback);
	}

	static void keyboardCallback(unsigned char key, int x, int y)
	{
		keyboardInstance->keyboard(key, x, y);
	}

	void keyboard(unsigned char key, int x, int y)
	{
		// Esc key
		if (key == 27)
		{
			glutLeaveMainLoop();
			return;
		}
		else if (key == '1')
		{
			rendererInst->adjustEyeSeperation(true);
		}
		else if (key == '2')
		{
			rendererInst->adjustEyeSeperation(false);
		}
		else if (key == '3')
		{
			rendererInst->adjustConvergenceDistance(true);
		}
		else if (key == '4')
		{
			rendererInst->adjustConvergenceDistance(false);
		}

		// Color anaglyph
		else if (key == '5')
		{
			rendererInst->anaglyphColors.setActiveColorAnaglyph();
			rendererInst->updateActiveAnaglyphClr();
		}
		// Half color anaglyph
		else if (key == '6')
		{
			rendererInst->anaglyphColors.setActiveHalfColorAnaglyph();
			rendererInst->updateActiveAnaglyphClr();
		}
		// Optimized color anaglyph
		else if (key == '7')
		{
			rendererInst->anaglyphColors.setActiveOptimizedAnaglyph();
			rendererInst->updateActiveAnaglyphClr();

		}
		// Grey color anaglyph
		else if (key == '8')
		{
			rendererInst->anaglyphColors.setActiveGreyAnaglyph();
			rendererInst->updateActiveAnaglyphClr();
		}
		// True color anaglyph
		else if (key == '9')
		{
			rendererInst->anaglyphColors.setActiveTrueAnaglyph();
			rendererInst->updateActiveAnaglyphClr();
		}
		else if (key == '0')
		{
			rendererInst->anaglyphColors.setActiveGIMPColorScheme();
			rendererInst->updateActiveAnaglyphClr();

		}


		// w
		else if (key == 119)
		{
			rendererInst->camFwdCallback();
		//	glutPostRedisplay();
			return;
		}
		// s
		else if (key == 115)
		{
			rendererInst->camBwdCallback();
		//	glutPostRedisplay();
			return;
		}
		// a
		else if (key == 97)
		{
			rendererInst->camLeftCallback();
		//	glutPostRedisplay();
			return;
		}
		// d
		else if (key == 100)
		{
			rendererInst->camRightCallback();
			//glutPostRedisplay();
			return;
		}

		
		else if (key == 'i')
		{
			rendererInst->camPitchCallback(5.0f);
			//	glutPostRedisplay();
			return;
		}
		// s
		else if (key == 'k')
		{
			rendererInst->camPitchCallback(-5.0f);
			//	glutPostRedisplay();
			return;
		}
		// a
		else if (key == 'j')
		{
			rendererInst->camYawCallback(5.0f);
			//	glutPostRedisplay();
			return;
		}
		// d
		else if (key == 'l')
		{
			rendererInst->camYawCallback(-5.0f);
			//glutPostRedisplay();
			return;
		}

		else if (key == 'f')
		{
			windowInstance->toggleFullScreen();
		}

		else if (key == 'x')
		{
			rendererInst->useFXAA = !rendererInst->useFXAA;
		}
		else if (key == 'm')
		{
			rendererInst->lookAtDefault();
		}
		
		

	}



};