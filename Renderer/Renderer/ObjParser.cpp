#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include "RenderObject.cpp"

using namespace std;

class ObjParser
{
public:
	ObjParser()
	{

	}

	string getRelativePath(string path)
	{
		string strFilename = __FILE__;
		string::size_type pos = strFilename.find_last_of("\\/");
		strFilename = strFilename.substr(0, pos);

	//	strFilename = "";

		strFilename += path;

		return strFilename;
	}


	void loadObj(string filename, RenderObject *obj) 
	{
		bool vtPresent = false;
		filename = getRelativePath(filename);
		
		ifstream in(filename, ios::in);
		vector<glm::vec4> vertices; 
		vector<GLuint> elements;


		if (!in) { cerr << "Cannot open " << filename << endl; exit(1); }


		string line;
		while (getline(in, line)) 
		{
			if (line.substr(0, 2) == "v ") 
			{
				istringstream s(line.substr(2));
				glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0f;
				vertices.push_back(v);
			}
			else if (line.substr(0, 2) == "f ") 
			{
				istringstream s(line.substr(2));
				GLushort a, b, c;
				char skip;
				int skipNorm;

				
				s >> a; 
				
				s >> skip;
				
				if (vtPresent == true)
				{
					s >> skipNorm;
				}

				s >> skip;
				s >> skipNorm;

				s >> b; 
				
				s >> skip;

				if (vtPresent == true)
				{
					s >> skipNorm;
				}

				s >> skip;
				s >> skipNorm;
				
				s >> c;



				a--; b--; c--;
				elements.push_back(a); elements.push_back(b); elements.push_back(c);
			}
			else if (line.substr(0, 2) == "vt")
			{
				vtPresent = true;
			}
			else if (line[0] == '#') 
			{ /* ignoring this line */ }
			else 
			{ /* ignoring this line */ }
		}

	//	cout << vertices.size() << " " << elements.size() << endl;

		vector<glm::vec3> normals(vertices.size());

		for (int i = 0; i < elements.size(); i = i + 3) {
			GLuint ia = elements[i];
			GLuint ib = elements[i + 1];
			GLuint ic = elements[i + 2];


			glm::vec3 normal = glm::normalize(glm::cross(
				glm::vec3(vertices[ib]) - glm::vec3(vertices[ia]),
				glm::vec3(vertices[ic]) - glm::vec3(vertices[ia])));
			normals[ia] = normals[ib] = normals[ic] = normal;
		}

		obj->addPositionData(elements);

		

		vector<float> data;
		obj->dataOffset = 0;

		for (int i = 0; i < vertices.size(); i++)
		{
			data.push_back(vertices[i].x);
			data.push_back(vertices[i].y);
			data.push_back(vertices[i].z);
			data.push_back(vertices[i].w);

		}

		obj->normalsOffset = data.size() * sizeof(float);

		for (int i = 0; i < normals.size(); i++)
		{
			data.push_back(normals[i].x);
			data.push_back(normals[i].y);
			data.push_back(normals[i].z);
		}


		obj->textureCoordOffset = data.size() * sizeof(float);

		for (int i = 0; i < vertices.size(); i = i + 4)
		{
				data.push_back(0.0f);
				data.push_back(0.0f);

				data.push_back(1.0f);
				data.push_back(0.0f);

				data.push_back(1.0f);
				data.push_back(1.0f);

				data.push_back(0.0f);
				data.push_back(1.0f);

		}

		obj->addVertexData(data);


	}


};